package cz.zcu.kiv.iszamobile.iszamobile.service;

public interface NotificationServiceApi
{


    void sendNotification(String deviceRegistrationId, String title, String body);
}
