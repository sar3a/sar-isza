package cz.zcu.kiv.iszamobile.iszamobile.dto;

import cz.zcu.kiv.iszamobile.iszamobile.repository.AbstractDao;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HlaseniDto extends AbstractDto{

    Long idHlaseni;
    Long cisloHlaseni; // 10557, 11667,6050
    Long rokHlaseni;  //2016, 2017
    String druhHlaseni; //napr. VADA, POZADAVEK, NAMET
    Date datumVyrizeniHlaseni;
    String nazev; // napr.: Posun terminu - chyb. hlaska, Standardni zakazky, ISZA - garant modulu
    String textHlaseni;
    String kodUzivateleResi; // budeme potrebovat id resitele
    SystemDto system;

    @Override
    public AbstractDao mapToDao() {
        return null;
    }
}
