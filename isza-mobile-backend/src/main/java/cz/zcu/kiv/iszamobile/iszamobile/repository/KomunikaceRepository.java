package cz.zcu.kiv.iszamobile.iszamobile.repository;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface KomunikaceRepository extends CrudRepository<Komunikace, Long> {

    List<Komunikace> findAllByIdHlaseniOrderByDatumVlozeniAsc(Long idHlaseni);

}
