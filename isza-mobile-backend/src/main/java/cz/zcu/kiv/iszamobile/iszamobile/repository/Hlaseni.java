package cz.zcu.kiv.iszamobile.iszamobile.repository;


import cz.zcu.kiv.iszamobile.iszamobile.dto.AbstractDto;
import cz.zcu.kiv.iszamobile.iszamobile.dto.HlaseniDto;
import cz.zcu.kiv.iszamobile.iszamobile.dto.SystemDto;
import lombok.Data;

import javax.persistence.*;
import java.sql.Date;

@Table(name = "ccag_hlaseni")
@Entity
@Data
public class Hlaseni extends AbstractDao {

    @Column(name = "id_hlaseni")
    @Id
    Long idHlaseni;

    @Column(name = "cislo_hlaseni")
    Long cisloHlaseni; // 10557, 11667,6050

    @Column(name = "rok_hlaseni")
    Long rokHlaseni;  //2016, 2017

    @Column(name = "druh_hlaseni")
    String druhHlaseni; //napr. VADA, POZADAVEK, NAMET

    @Column(name = "datum_vyrizeni_hlaseni")
    Date datumVyrizeniHlaseni;

    @Basic
    String nazev; // napr.: Po sun terminu - chyb. hlaska, Standardni zakazky, ISZA - garant modulu

    @Column(name="text_hlaseni")
    String textHlaseni;

    @Column(name = "kod_uzivatele_resi")
    String kodUzivateleResi; // budeme potrebovat id resitele

    @OneToOne(fetch=FetchType.EAGER)
    @JoinColumn(name = "id_systemu")
    System system;

    @Override
    public HlaseniDto mapToDto() {
        HlaseniDto hlaseni = new HlaseniDto();
        hlaseni.setIdHlaseni(this.getIdHlaseni());
        hlaseni.setCisloHlaseni(this.getCisloHlaseni());
        hlaseni.setRokHlaseni(this.getRokHlaseni());
        hlaseni.setDruhHlaseni(this.getDruhHlaseni());
        hlaseni.setDatumVyrizeniHlaseni(this.getDatumVyrizeniHlaseni());
        hlaseni.setNazev(this.getNazev());
        hlaseni.setTextHlaseni(this.getTextHlaseni());
        hlaseni.setKodUzivateleResi(this.getKodUzivateleResi());
//        hlaseni.setIdSystemu(this.getIdSystemu());
        hlaseni.setSystem(this.getSystem().mapToDto());
        return hlaseni;
    }
}
