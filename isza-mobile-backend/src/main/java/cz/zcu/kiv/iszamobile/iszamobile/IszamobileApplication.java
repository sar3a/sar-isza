package cz.zcu.kiv.iszamobile.iszamobile;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IszamobileApplication {

    public static void main(String[] args) {
        SpringApplication.run(IszamobileApplication.class, args);
    }
}
