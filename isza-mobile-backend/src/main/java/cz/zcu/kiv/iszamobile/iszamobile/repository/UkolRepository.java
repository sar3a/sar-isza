package cz.zcu.kiv.iszamobile.iszamobile.repository;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UkolRepository extends CrudRepository<Ukol,Long> {

    List<Ukol> findAllByIdDohlizejici(Long idDohlizejici);

    List<Ukol> findAllByIdResitel(Long idResitel);

}
