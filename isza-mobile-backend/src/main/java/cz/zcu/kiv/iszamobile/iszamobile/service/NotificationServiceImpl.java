package cz.zcu.kiv.iszamobile.iszamobile.service;



import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import us.raudi.pushraven.FcmResponse;
import us.raudi.pushraven.Notification;
import us.raudi.pushraven.Pushraven;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

@Service
public class NotificationServiceImpl implements NotificationServiceApi{


    private static final String FCM_SERVER_API_KEY = "AAAAu_jOZAg:APA91bE4jTQ_K691TXect5CGIkmg3eQRmjAyKg8juFZnxAN3T1wOQjcaK-KmnvaTauTL5qW2MhaqqUgH59KdMbr2kdlL4hDj_izCa5F0Q0Ywr1iSdChyZbe4iwCxiolRiTJlE3nRwndT";
    private static final String FCM_URL = "https://fcm.googleapis.com/fcm/send";

    protected HttpURLConnection getConnection(String url) throws IOException {
        String fcmUrl = "https://fcm.googleapis.com/fcm/send";
        return (HttpURLConnection) new URL(fcmUrl).openConnection();
    }


    @Override
    @Async
    public void sendNotification(String deviceRegistrationId, String title, String body) {
        Pushraven.setKey(FCM_SERVER_API_KEY);
        Notification raven = new Notification()
                .title(title)
                .text(body)
                .to(deviceRegistrationId);
        Pushraven.setNotification(raven);
        FcmResponse response = Pushraven.push(raven);
        int pom = 0;
    }



}
