package cz.zcu.kiv.iszamobile.iszamobile.controllers;

import cz.zcu.kiv.iszamobile.iszamobile.dto.*;
import cz.zcu.kiv.iszamobile.iszamobile.repository.AbstractDao;
import cz.zcu.kiv.iszamobile.iszamobile.repository.Osoba;
import cz.zcu.kiv.iszamobile.iszamobile.repository.Ukol;
import cz.zcu.kiv.iszamobile.iszamobile.service.IszaServiceApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
public class IszaController {


    @Autowired
    IszaServiceApi iszaServiceApi;



    @RequestMapping(value="/nastavTokenUzivatele", method = RequestMethod.POST)
    public OsobaDto nastavTokenUzivatele(@RequestBody OsobaDto osoba){
        return  iszaServiceApi.nastavTokenUzivatele(osoba.mapToDao());
    }

    @RequestMapping(value = "/seznamOsob", method = RequestMethod.GET)
    public Collection<OsobaDto> seznamOsob(){
        return iszaServiceApi.seznamOsob().stream().map(dao->dao.mapToDto()).collect(Collectors.toList());
    }

    @RequestMapping(value = "/testNotification/{idOsoby}", method = RequestMethod.GET)
    public void sendTestNotificationOsob(Long idOsoby){
        iszaServiceApi.sendNotification(idOsoby);
    }

    @RequestMapping(value = "/prihlasUzivatele/{kodOsoby}", method = RequestMethod.GET)
    public OsobaDto prihlasUzivatele(@PathVariable("kodOsoby") String kodOsoby){
        Osoba osoba = iszaServiceApi.prihlasOsobu(kodOsoby);
        return osoba == null ? null : osoba.mapToDto();
    }

    @RequestMapping(value = "/otevrenaHlaseni", method = RequestMethod.GET)
    public Collection<HlaseniDto> otevrenaHlaseni() {
        return iszaServiceApi.getOtevrenaHlaseni().stream().map(dao->dao.mapToDto()).collect(Collectors.toList());
    }

    @RequestMapping(value = "/vytvorKomunikaci", method = RequestMethod.POST)
    public KomunikaceDto vytvorKomunikaci(@RequestBody KomunikaceDto komunikace) {
        return iszaServiceApi.vytvorKomunikaci(komunikace.mapToDao()).mapToDto();
    }

    @RequestMapping(value = "/vytvorUkol", method = RequestMethod.POST)
    public UkolDto vytvorUkol(@RequestBody UkolDto ukolDto) {
        Ukol ukol = iszaServiceApi.vytvorUkol(ukolDto.mapToDao());
        return ukol == null ? null : ukol.mapToDto();
    }

    @RequestMapping(value = "/komunikaceHlaseni/{idHlaseni}", method = RequestMethod.GET)
    public Collection<KomunikaceDto> komunikaceUzivatele(@PathVariable("idHlaseni") Long idHlaseni) {
        return iszaServiceApi.komunikaceHlaseni(idHlaseni).stream().map(dao->dao.mapToDto()).collect(Collectors.toList());
    }

    @RequestMapping(value = "/ukolyDohlizeneUzivatelem/{idUzivatele}", method = RequestMethod.GET)
    public List<UkolDto> ukolyDohlizeneUzivatelem(@PathVariable("idUzivatele") Long idUzivatele) {
        return iszaServiceApi.ukolyDohlizeneUzivatelem(idUzivatele).stream().map(dao -> dao.mapToDto()).collect(Collectors.toList());
    }

    @RequestMapping(value = "/ukolyZadaneUzivateli/{idUzivatele}", method = RequestMethod.GET)
    public List<UkolDto> ukolyZadaneUzivateli(@PathVariable("idUzivatele") Long idUzivatele) {
        return iszaServiceApi.ukolyZadaneUzivateli(idUzivatele).stream().map(dao -> dao.mapToDto()).collect(Collectors.toList());
    }

    @RequestMapping(value = "/nastavStavUkolu", method = RequestMethod.POST)
    public UkolDto nastavStavUkolu(@RequestBody UkolDto ukolDto) {
        Ukol ukol = iszaServiceApi.nastavStavUkolu(ukolDto.mapToDao());
        return ukol == null ? null : ukol.mapToDto();
    }


}
