package cz.zcu.kiv.iszamobile.iszamobile.service;

import cz.zcu.kiv.iszamobile.iszamobile.dto.OsobaDto;
import cz.zcu.kiv.iszamobile.iszamobile.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.lang.System;
import java.sql.Date;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class IszaServiceImpl implements IszaServiceApi {

    @Autowired
    FirmaRepository firmaRepository;

    @Autowired
    KomunikaceRepository komunikaceRepository;

    @Autowired
    UkolRepository ukolRepository;

    @Autowired
    HlaseniRepository hlaseniRepository;

    @Autowired
    SystemRepository systemRepository;

    @Autowired
    OsobaRepository osobaRepository;

    @Autowired
    NotificationServiceApi notificationServiceApi;

    private Map<Long, String> tokenyUzivatelu = new HashMap<>();

    @Override
    public List<Hlaseni> getOtevrenaHlaseni() {
        List<Hlaseni> hlaseni = hlaseniRepository.findAllByDatumVyrizeniHlaseniIsNull();
        return hlaseni;
    }

    @Override
    @Transactional
    public Komunikace vytvorKomunikaci(Komunikace komunikace) {
        Komunikace ulozenaKomunikace = komunikaceRepository.save(komunikace);
        return ulozenaKomunikace;
    }

    @Override
    public Osoba prihlasOsobu(String kodOsoby) {
        //TODO
        Osoba osoba = osobaRepository.findByKodOsoby(kodOsoby);
        return osoba;
    }

    private String ziskejJmenoOsoby(Long idOsoby){
        Osoba dohlizejici = osobaRepository.findOne(idOsoby);
        return dohlizejici.getJmeno()+" "+dohlizejici.getPrijmeni();
    }

    private String vyrobSnippetTextuUkolu(Ukol ukol){
        return ukol.getPopis().substring(0,35)+" ...";
    }

    @Override
    public Ukol vytvorUkol(Ukol ukol) {
        ukol.setIdUkolu(0L);
        ukol.setJePrijat(false);
        Ukol ulozenyUkol = ukolRepository.save(ukol);

        //posli notifikaci resiteli !!!
        String title = "Novy ukol od: "+ ziskejJmenoOsoby(ukol.getIdDohlizejici());
        String body  = vyrobSnippetTextuUkolu(ukol);
        notificationServiceApi.sendNotification(tokenyUzivatelu.get(ukol.getIdResitel()),title,body);

        return ulozenyUkol;
    }

    @Override
    public List<Komunikace> komunikaceHlaseni(String kodUzivatele) {
        //TODO implementace po vyjasneni
        return null;
    }

    @Override
    public List<Komunikace> komunikaceHlaseni(Long idHlaseni) {
        return komunikaceRepository.findAllByIdHlaseniOrderByDatumVlozeniAsc(idHlaseni);
    }

    @Override
    public List<Ukol> ukolyDohlizeneUzivatelem(Long idUzivatele) {
        List<Ukol> ukoly = ukolRepository.findAllByIdDohlizejici(idUzivatele);
        return ukoly;
    }

    @Override
    public List<Ukol> ukolyZadaneUzivateli(Long IdUzivatele) {
        List<Ukol> ukoly = ukolRepository.findAllByIdResitel(IdUzivatele);
        return ukoly;
    }

    @Override
    public Ukol nastavStavUkolu(Ukol ukol) {
        Ukol ukolRefreshed = ukolRepository.findOne(ukol.getIdUkolu());
        ukolRefreshed.setJePrijat(ukol.isJePrijat());

        String title = ziskejJmenoOsoby(ukolRefreshed.getIdResitel())+" "+ (ukolRefreshed.isJePrijat() ? "prijal": "odmitl")+" ukol:";
        String body = vyrobSnippetTextuUkolu(ukolRefreshed);
        notificationServiceApi.sendNotification(tokenyUzivatelu.get(ukolRefreshed.getIdDohlizejici()),title,body);
        return ukolRepository.save(ukolRefreshed);
    }


    public Date getCurrentDate() {
        return new Date(Calendar.getInstance().getTime().getTime());
    }

    @Override
    public void test() {
        Komunikace komunikace = komunikaceRepository.findAll().iterator().next();
    }

    @Override
    public Set<Osoba> seznamOsob() {
        return osobaRepository.findAllByIdOsobyIsNotNull().stream().collect(Collectors.toSet());
    }

    @Override
    public OsobaDto nastavTokenUzivatele(Osoba osoba) {
        tokenyUzivatelu.put(osoba.getIdOsoby(),osoba.getFirebaseToken());
        System.out.println("Prijat token, osoba id: "+osoba.getIdOsoby()+ "  token: "+osoba.getFirebaseToken());
        OsobaDto pom =  new OsobaDto();
        pom.setIdOsoby(0L);
        return pom;
    }

    @Override
    public void sendNotification(Long idOsoby) {
        String pomToken = "cWcLY1Yo144:APA91bGXW2My-VTNYrHzE3GYzFWhWt86PM7fe5LtEpk6Er9y612nLqx_Dk7CJ3gcduvz_9VqKPOUohwqNVQBNmVma5fcEjvfTZXO6XV9X0uedRtcbSNkVzEpsU0hGl3Fo441-5Tz8Skg";
        tokenyUzivatelu.put(1L, pomToken);

//        notificationServiceApi.sendNotification(pomToken,"Title 1","Jsi borec fest");
    }
}
