package cz.zcu.kiv.iszamobile.iszamobile.dto;

import cz.zcu.kiv.iszamobile.iszamobile.repository.AbstractDao;

import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractDto {

    public abstract AbstractDao mapToDao();

    public static List<? extends AbstractDao> mapDtoListToDao(List<? extends AbstractDto> dtoList){
        return dtoList.stream().map(dto -> dto.mapToDao()).collect(Collectors.toList());
    }
}
