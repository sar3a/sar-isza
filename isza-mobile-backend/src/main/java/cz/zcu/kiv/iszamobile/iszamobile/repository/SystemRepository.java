package cz.zcu.kiv.iszamobile.iszamobile.repository;

import org.springframework.data.repository.CrudRepository;

public interface SystemRepository extends CrudRepository<System, Long> {
}
