package cz.zcu.kiv.iszamobile.iszamobile.repository;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface HlaseniRepository extends CrudRepository<Hlaseni, Long> {

    List<Hlaseni> findAllByDatumVyrizeniHlaseniIsNull();
}
