package cz.zcu.kiv.iszamobile.iszamobile.service;

import cz.zcu.kiv.iszamobile.iszamobile.dto.OsobaDto;
import cz.zcu.kiv.iszamobile.iszamobile.repository.Hlaseni;
import cz.zcu.kiv.iszamobile.iszamobile.repository.Komunikace;
import cz.zcu.kiv.iszamobile.iszamobile.repository.Osoba;
import cz.zcu.kiv.iszamobile.iszamobile.repository.Ukol;

import java.util.List;
import java.util.Set;

public interface IszaServiceApi {

    List<Hlaseni> getOtevrenaHlaseni();

    Komunikace vytvorKomunikaci(Komunikace komunikace);

    Osoba prihlasOsobu(String kodUzivatele);

    Ukol vytvorUkol(Ukol ukol);

    List<Komunikace> komunikaceHlaseni(String kodUzivatele);

    List<Komunikace> komunikaceHlaseni(Long idHlaseni);

    List<Ukol> ukolyDohlizeneUzivatelem(Long idUzivatele);

    List<Ukol> ukolyZadaneUzivateli(Long IdUzivatele);

    Ukol nastavStavUkolu(Ukol ukol);

    void test();

    Set<Osoba> seznamOsob();

    OsobaDto nastavTokenUzivatele(Osoba osoba);

    void sendNotification(Long idOsoby);

}
