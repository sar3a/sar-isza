package cz.zcu.kiv.iszamobile.iszamobile.dto;

import cz.zcu.kiv.iszamobile.iszamobile.repository.Osoba;
import lombok.Data;

@Data
public class OsobaDto extends AbstractDto{

    Long idOsoby;
    String jmeno;
    String prijmeni;
    FirmaDto firma;
    String kodOsoby;
    boolean jeZamestnanec;
    String firebaseToken;

    @Override
    public Osoba mapToDao() {
        Osoba osoba = new Osoba();

        osoba.setIdOsoby(this.getIdOsoby());
        osoba.setJmeno(this.getJmeno());
        osoba.setPrijmeni(this.getPrijmeni());
        osoba.setIdFirmy(this.getFirma()==null? null : this.getFirma().getIdFirmy());
        osoba.setKodOsoby(this.kodOsoby);
        osoba.setJeZamestnanec(this.isJeZamestnanec());
        osoba.setFirebaseToken(this.getFirebaseToken());
        return osoba;
    }
}
