package cz.zcu.kiv.iszamobile.iszamobile.dto;

import cz.zcu.kiv.iszamobile.iszamobile.repository.Ukol;
import lombok.Data;

import java.sql.Date;

@Data
public class UkolDto extends AbstractDto{
    Long idUkolu;
    String popis;
//    String resitel;
    Date terminSplneni;
//    Date datumSplneni;
    boolean jePrijat;
    Long idDohlizejici;
    Long idResitel;

    @Override
    public Ukol mapToDao() {
        Ukol ukol = new Ukol();

        ukol.setIdUkolu(this.getIdUkolu());
        ukol.setPopis(this.getPopis());
//        ukol.setResitel(this.getResitel());
        ukol.setTerminSplneni(this.getTerminSplneni());
//        ukol.setDatumSplneni(this.getDatumSplneni());
        ukol.setJePrijat(this.isJePrijat());
        ukol.setIdDohlizejici(this.getIdDohlizejici());
        ukol.setIdResitel(this.getIdResitel());

        return ukol;
    }
}
