package cz.zcu.kiv.iszamobile.iszamobile.dto;

import cz.zcu.kiv.iszamobile.iszamobile.repository.AbstractDao;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SystemDto extends AbstractDto{
    Long idSystemu;
    String kodSystemu;
    String poznamka;
    String priznakAnZivy;

    @Override
    public AbstractDao mapToDao() {
        return null;
    }
}
