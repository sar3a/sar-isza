package cz.zcu.kiv.iszamobile.iszamobile.dto;


import cz.zcu.kiv.iszamobile.iszamobile.repository.Komunikace;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class KomunikaceDto extends AbstractDto{

    Long idKomunikace;
//    Long poradoveCislo;
    String obsahKomunikace;
//    String idZakaznika;
    Long idPuvodceKomunikace;
    Date datumVlozeni;
    Long idHlaseni;

    @Override
    public Komunikace mapToDao() {
        Komunikace komunikace = new Komunikace();

        komunikace.setIdKomunikace(this.getIdKomunikace());
        komunikace.setObsahKomunikace(this.getObsahKomunikace());
        komunikace.setIdPuvodceKomunikace(this.getIdPuvodceKomunikace());
        komunikace.setDatumVlozeni(this.getDatumVlozeni());
        komunikace.setPoradoveCislo(0L);
//        komunikace.setPoradoveCislo(this.getPoradoveCislo());
//        komunikace.setIdZakaznika(this.getIdZakaznika());
        komunikace.setIdHlaseni(this.getIdHlaseni());
        return komunikace;
    }
}
