package cz.zcu.kiv.iszamobile.iszamobile.repository;

import cz.zcu.kiv.iszamobile.iszamobile.dto.UkolDto;
import lombok.Data;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "ccag_ukol")
@Data
public class Ukol extends AbstractDao{

    @Id
    @Column(name = "id_ukol")
    @GeneratedValue(strategy = GenerationType.TABLE, generator="ukol_id_generator")
    @TableGenerator(
            schema="ccag",
            table="ccag_pk_id_generator",
            allocationSize = 1,
            pkColumnName = "table_name",
            pkColumnValue = "ccag_ukol",
            valueColumnName = "next_id",
            name = "ukol_id_generator")
    Long idUkolu;

    @Basic
    String popis;

    @Basic
    String resitel;

    @Column(name="termin_splneni")
    Date terminSplneni;

//    @Column(name="datum_splneni")
//    Date datumSplneni;

    @Column(name="je_prijat")
    boolean jePrijat;

    @Column(name="id_dohlizejici")
    Long idDohlizejici; // id zadavajici osoby

    @Column(name="id_resitel")
    Long idResitel; // id resitele osoby

    @Override
    public UkolDto mapToDto() {
        UkolDto ukol = new UkolDto();

        ukol.setIdUkolu(this.getIdUkolu());
        ukol.setPopis(this.getPopis());
//        ukol.setResitel(this.getResitel());
        ukol.setTerminSplneni(this.getTerminSplneni());
//        ukol.setDatumSplneni(this.getDatumSplneni());
        ukol.setJePrijat(this.isJePrijat());
        ukol.setIdDohlizejici(this.getIdDohlizejici());
        ukol.setIdResitel(this.getIdResitel());
        return ukol;
    }
}
