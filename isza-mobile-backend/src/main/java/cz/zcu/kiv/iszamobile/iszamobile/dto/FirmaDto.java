package cz.zcu.kiv.iszamobile.iszamobile.dto;

import lombok.Data;

@Data
public class FirmaDto {
    Long idFirmy;
    String nazev;
}
