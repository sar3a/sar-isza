package cz.zcu.kiv.iszamobile.iszamobile.repository;

import org.springframework.data.repository.CrudRepository;

import java.math.BigDecimal;
import java.util.List;

public interface FirmaRepository extends CrudRepository<Firma, Long> {


}
