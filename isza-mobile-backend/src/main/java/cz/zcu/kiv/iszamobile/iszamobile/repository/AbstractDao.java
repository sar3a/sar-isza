package cz.zcu.kiv.iszamobile.iszamobile.repository;

import cz.zcu.kiv.iszamobile.iszamobile.dto.AbstractDto;

public abstract class AbstractDao {

    public abstract AbstractDto mapToDto();

}
