package cz.zcu.kiv.iszamobile.iszamobile.repository;


import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name="ccag_firma")
@Data
public class Firma {

    @Id
    @Column(name ="id_firmy")
    Long idFirmy;

    @Basic
    String nazev;

}
