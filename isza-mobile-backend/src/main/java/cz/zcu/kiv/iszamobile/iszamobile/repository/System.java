package cz.zcu.kiv.iszamobile.iszamobile.repository;

import cz.zcu.kiv.iszamobile.iszamobile.dto.AbstractDto;
import cz.zcu.kiv.iszamobile.iszamobile.dto.SystemDto;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "ccag_systemy")
@Data
public class System extends AbstractDao{


    @Id
    @Column(name = "id_systemu")
    Long idSystemu;

    @Column(name="kod_systemu")
    String kodSystemu;

    @Basic
    String poznamka;

    @Column(name="priznak_an_zivy")
    String priznakAnZivy;

    @Override
    public SystemDto mapToDto() {
        SystemDto system = new SystemDto();
        system.setIdSystemu(this.getIdSystemu());
        system.setKodSystemu(this.getKodSystemu());
        system.setPoznamka(this.getPoznamka());
        system.setPriznakAnZivy(this.getPriznakAnZivy());

        return system;
    }
}
