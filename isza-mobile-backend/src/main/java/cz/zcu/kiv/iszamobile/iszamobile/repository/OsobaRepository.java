package cz.zcu.kiv.iszamobile.iszamobile.repository;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface OsobaRepository extends CrudRepository<Osoba, Long> {

    List<Osoba> findAllByIdOsobyIsNotNull();

    Osoba findByKodOsoby(String kodOsoby);
}
