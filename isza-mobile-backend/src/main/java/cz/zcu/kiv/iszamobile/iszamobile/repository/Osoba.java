package cz.zcu.kiv.iszamobile.iszamobile.repository;

import cz.zcu.kiv.iszamobile.iszamobile.dto.AbstractDto;
import cz.zcu.kiv.iszamobile.iszamobile.dto.OsobaDto;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "ccag_osoba")
@Data
public class Osoba extends AbstractDao{

    @Id
    @Column(name = "id_osoby")
    Long idOsoby;

    @Basic
    String jmeno;

    @Basic
    String prijmeni;

    @Column(name = "id_firmy")
    Long idFirmy;

    @Column(name="kod_osoby")
    String kodOsoby; // Pro prihlaseni

    @Column(name="je_zamestnanec")
    boolean jeZamestnanec;

    @Transient
    String firebaseToken;

    @Override
    public OsobaDto mapToDto() {
        OsobaDto osoba = new OsobaDto();
        osoba.setIdOsoby(this.getIdOsoby());
        osoba.setJmeno(this.getJmeno());
        osoba.setPrijmeni(this.getPrijmeni());
        osoba.setFirma(null);
        osoba.setKodOsoby(this.getKodOsoby());
        osoba.setJeZamestnanec(this.isJeZamestnanec());
        osoba.setFirebaseToken(this.getFirebaseToken());
        return osoba;
    }
}
