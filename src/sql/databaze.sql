--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.3
-- Dumped by pg_dump version 10.0

-- Started on 2017-10-17 12:09:12

DROP DATABASE IF EXISTS iszadb;

DROP USER IF EXISTS ccag;

DROP USER IF EXISTS ccag_apl;

CREATE USER ccag;

CREATE USER ccag_apl;

CREATE DATABASE iszadb;

\connect iszadb;

--DROP SCHEMA IF EXISTS ccag CASCADE;

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 7 (class 2615 OID 17077)
-- Name: ccag; Type: SCHEMA; Schema: -; Owner: ccag
--

CREATE SCHEMA ccag;


ALTER SCHEMA ccag OWNER TO ccag;

--
-- TOC entry 1 (class 3079 OID 12387)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2184 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = ccag, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 186 (class 1259 OID 17078)
-- Name: ccag_firma; Type: TABLE; Schema: ccag; Owner: ccag
--

CREATE TABLE ccag_firma (
    id_firmy numeric NOT NULL,
    nazev character varying(255) NOT NULL
);


ALTER TABLE ccag_firma OWNER TO ccag;

--
-- TOC entry 2185 (class 0 OID 0)
-- Dependencies: 186
-- Name: TABLE ccag_firma; Type: COMMENT; Schema: ccag; Owner: ccag
--

COMMENT ON TABLE ccag_firma IS 'Firmy';


--
-- TOC entry 2186 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN ccag_firma.id_firmy; Type: COMMENT; Schema: ccag; Owner: ccag
--

COMMENT ON COLUMN ccag_firma.id_firmy IS 'Identifikátor firmy';


--
-- TOC entry 2187 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN ccag_firma.nazev; Type: COMMENT; Schema: ccag; Owner: ccag
--

COMMENT ON COLUMN ccag_firma.nazev IS 'Název firmy';


--
-- TOC entry 187 (class 1259 OID 17084)
-- Name: ccag_hlaseni; Type: TABLE; Schema: ccag; Owner: ccag
--

CREATE TABLE ccag_hlaseni (
    id_hlaseni numeric NOT NULL,
    cislo_hlaseni numeric(10,0) NOT NULL,
    rok_hlaseni numeric(4,0) NOT NULL,
    druh_hlaseni character varying(20) NOT NULL,
    datum_vzniku date NOT NULL,
    id_firmy numeric NOT NULL,
    id_zakaznika numeric NOT NULL,
    termin date,
    pracnost numeric,
    text_hlaseni character varying(4000) NOT NULL,
    text_vyrizeni character varying(4000),
    kod_uzivatele_resi character varying(30),
    kod_uzivatele_vyridil character varying(30),
    datum_vyrizeni_hlaseni date,
    kodpra_i character varying(30) NOT NULL,
    datum_i date NOT NULL,
    kodpra_u character varying(30),
    datum_u date,
    kod_uzivatele_garant character varying(30),
    kod_uzivatele_vyridil_garant character varying(30),
    datum_vyrizeni_hlaseni_garant date,
    priorita numeric(1,0),
    nazev character varying(255),
    id_systemu numeric
);


ALTER TABLE ccag_hlaseni OWNER TO ccag;

--
-- TOC entry 2188 (class 0 OID 0)
-- Dependencies: 187
-- Name: TABLE ccag_hlaseni; Type: COMMENT; Schema: ccag; Owner: ccag
--

COMMENT ON TABLE ccag_hlaseni IS 'Tabulka hlášení od zákazníků, nebo pracovníků CCA';


--
-- TOC entry 2189 (class 0 OID 0)
-- Dependencies: 187
-- Name: COLUMN ccag_hlaseni.id_hlaseni; Type: COMMENT; Schema: ccag; Owner: ccag
--

COMMENT ON COLUMN ccag_hlaseni.id_hlaseni IS 'Jednoznačný identifikátor hlášení';


--
-- TOC entry 2190 (class 0 OID 0)
-- Dependencies: 187
-- Name: COLUMN ccag_hlaseni.cislo_hlaseni; Type: COMMENT; Schema: ccag; Owner: ccag
--

COMMENT ON COLUMN ccag_hlaseni.cislo_hlaseni IS 'Číslo hlášení v rámci roku. Spolu s rokem hlášení je unikátní';


--
-- TOC entry 2191 (class 0 OID 0)
-- Dependencies: 187
-- Name: COLUMN ccag_hlaseni.rok_hlaseni; Type: COMMENT; Schema: ccag; Owner: ccag
--

COMMENT ON COLUMN ccag_hlaseni.rok_hlaseni IS 'Rok, kdy hlášení vzniklo. Spolu s číslem hlášení je unikátní';


--
-- TOC entry 2192 (class 0 OID 0)
-- Dependencies: 187
-- Name: COLUMN ccag_hlaseni.druh_hlaseni; Type: COMMENT; Schema: ccag; Owner: ccag
--

COMMENT ON COLUMN ccag_hlaseni.druh_hlaseni IS 'Druh hlášení - VADA, POŽADAVEK, DOTAZ';


--
-- TOC entry 2193 (class 0 OID 0)
-- Dependencies: 187
-- Name: COLUMN ccag_hlaseni.datum_vzniku; Type: COMMENT; Schema: ccag; Owner: ccag
--

COMMENT ON COLUMN ccag_hlaseni.datum_vzniku IS 'Datum vzniku hlášení';


--
-- TOC entry 2194 (class 0 OID 0)
-- Dependencies: 187
-- Name: COLUMN ccag_hlaseni.id_firmy; Type: COMMENT; Schema: ccag; Owner: ccag
--

COMMENT ON COLUMN ccag_hlaseni.id_firmy IS 'Identifikátor firmy, která zanesla hlášení';


--
-- TOC entry 2195 (class 0 OID 0)
-- Dependencies: 187
-- Name: COLUMN ccag_hlaseni.id_zakaznika; Type: COMMENT; Schema: ccag; Owner: ccag
--

COMMENT ON COLUMN ccag_hlaseni.id_zakaznika IS 'Identifikátor osoby u zákazníka, která hlášení podala';


--
-- TOC entry 2196 (class 0 OID 0)
-- Dependencies: 187
-- Name: COLUMN ccag_hlaseni.termin; Type: COMMENT; Schema: ccag; Owner: ccag
--

COMMENT ON COLUMN ccag_hlaseni.termin IS 'Termín, do kdy má být hlášení splněno';


--
-- TOC entry 2197 (class 0 OID 0)
-- Dependencies: 187
-- Name: COLUMN ccag_hlaseni.pracnost; Type: COMMENT; Schema: ccag; Owner: ccag
--

COMMENT ON COLUMN ccag_hlaseni.pracnost IS 'Očekávaná pracnost na hlášení v ČD';


--
-- TOC entry 2198 (class 0 OID 0)
-- Dependencies: 187
-- Name: COLUMN ccag_hlaseni.text_hlaseni; Type: COMMENT; Schema: ccag; Owner: ccag
--

COMMENT ON COLUMN ccag_hlaseni.text_hlaseni IS 'Text hlášení';


--
-- TOC entry 2199 (class 0 OID 0)
-- Dependencies: 187
-- Name: COLUMN ccag_hlaseni.text_vyrizeni; Type: COMMENT; Schema: ccag; Owner: ccag
--

COMMENT ON COLUMN ccag_hlaseni.text_vyrizeni IS 'Text vyřízení hlášení';


--
-- TOC entry 2200 (class 0 OID 0)
-- Dependencies: 187
-- Name: COLUMN ccag_hlaseni.kod_uzivatele_resi; Type: COMMENT; Schema: ccag; Owner: ccag
--

COMMENT ON COLUMN ccag_hlaseni.kod_uzivatele_resi IS 'Kód uživatele (řešitel)';


--
-- TOC entry 2201 (class 0 OID 0)
-- Dependencies: 187
-- Name: COLUMN ccag_hlaseni.kod_uzivatele_vyridil; Type: COMMENT; Schema: ccag; Owner: ccag
--

COMMENT ON COLUMN ccag_hlaseni.kod_uzivatele_vyridil IS 'Kód uživatele vyřídil (řešitel)';


--
-- TOC entry 2202 (class 0 OID 0)
-- Dependencies: 187
-- Name: COLUMN ccag_hlaseni.datum_vyrizeni_hlaseni; Type: COMMENT; Schema: ccag; Owner: ccag
--

COMMENT ON COLUMN ccag_hlaseni.datum_vyrizeni_hlaseni IS 'Datum vyřízení hlášení (řešitel)';


--
-- TOC entry 2203 (class 0 OID 0)
-- Dependencies: 187
-- Name: COLUMN ccag_hlaseni.kodpra_i; Type: COMMENT; Schema: ccag; Owner: ccag
--

COMMENT ON COLUMN ccag_hlaseni.kodpra_i IS 'Kód uživatele, který záznam vložil';


--
-- TOC entry 2204 (class 0 OID 0)
-- Dependencies: 187
-- Name: COLUMN ccag_hlaseni.datum_i; Type: COMMENT; Schema: ccag; Owner: ccag
--

COMMENT ON COLUMN ccag_hlaseni.datum_i IS 'Datum poslední změny záznamu';


--
-- TOC entry 2205 (class 0 OID 0)
-- Dependencies: 187
-- Name: COLUMN ccag_hlaseni.kodpra_u; Type: COMMENT; Schema: ccag; Owner: ccag
--

COMMENT ON COLUMN ccag_hlaseni.kodpra_u IS 'Kód uživatele, který záznam jako poslední změnil';


--
-- TOC entry 2206 (class 0 OID 0)
-- Dependencies: 187
-- Name: COLUMN ccag_hlaseni.kod_uzivatele_garant; Type: COMMENT; Schema: ccag; Owner: ccag
--

COMMENT ON COLUMN ccag_hlaseni.kod_uzivatele_garant IS 'Kód uživatele (garant)';


--
-- TOC entry 2207 (class 0 OID 0)
-- Dependencies: 187
-- Name: COLUMN ccag_hlaseni.kod_uzivatele_vyridil_garant; Type: COMMENT; Schema: ccag; Owner: ccag
--

COMMENT ON COLUMN ccag_hlaseni.kod_uzivatele_vyridil_garant IS 'Kód uživatele vyřídil (garant)';


--
-- TOC entry 2208 (class 0 OID 0)
-- Dependencies: 187
-- Name: COLUMN ccag_hlaseni.datum_vyrizeni_hlaseni_garant; Type: COMMENT; Schema: ccag; Owner: ccag
--

COMMENT ON COLUMN ccag_hlaseni.datum_vyrizeni_hlaseni_garant IS 'Datum vyřízení hlášení (garant)';


--
-- TOC entry 2209 (class 0 OID 0)
-- Dependencies: 187
-- Name: COLUMN ccag_hlaseni.priorita; Type: COMMENT; Schema: ccag; Owner: ccag
--

COMMENT ON COLUMN ccag_hlaseni.priorita IS 'Priorita hlášení';


--
-- TOC entry 2210 (class 0 OID 0)
-- Dependencies: 187
-- Name: COLUMN ccag_hlaseni.nazev; Type: COMMENT; Schema: ccag; Owner: ccag
--

COMMENT ON COLUMN ccag_hlaseni.nazev IS 'Krátký název hlášení pro lepší orientaci';


--
-- TOC entry 2211 (class 0 OID 0)
-- Dependencies: 187
-- Name: COLUMN ccag_hlaseni.id_systemu; Type: COMMENT; Schema: ccag; Owner: ccag
--

COMMENT ON COLUMN ccag_hlaseni.id_systemu IS 'Identifikátor systému, ke kterému hlášení patří';


--
-- TOC entry 188 (class 1259 OID 17090)
-- Name: ccag_hlaseni_komunikace; Type: TABLE; Schema: ccag; Owner: ccag
--

CREATE TABLE ccag_hlaseni_komunikace (
    id_hlaseni numeric NOT NULL,
    poradove_cislo numeric NOT NULL,
    obsah_komunikace character varying(4000),
    id_zakaznika character varying(255),
    datum_vlozeni date NOT NULL,
    id_komunikace numeric NOT NULL,
	id_puvodce_komunikace numeric NOT NULL
);


ALTER TABLE ccag_hlaseni_komunikace OWNER TO ccag;

--
-- TOC entry 2212 (class 0 OID 0)
-- Dependencies: 188
-- Name: TABLE ccag_hlaseni_komunikace; Type: COMMENT; Schema: ccag; Owner: ccag
--

COMMENT ON TABLE ccag_hlaseni_komunikace IS 'Záznam komunikace se zákazníkem ke konkrétním hlášením';


--
-- TOC entry 2213 (class 0 OID 0)
-- Dependencies: 188
-- Name: COLUMN ccag_hlaseni_komunikace.id_hlaseni; Type: COMMENT; Schema: ccag; Owner: ccag
--

COMMENT ON COLUMN ccag_hlaseni_komunikace.id_hlaseni IS 'Identifikátor hlášení, ke kterému komunikace patří';


--
-- TOC entry 2214 (class 0 OID 0)
-- Dependencies: 188
-- Name: COLUMN ccag_hlaseni_komunikace.poradove_cislo; Type: COMMENT; Schema: ccag; Owner: ccag
--

COMMENT ON COLUMN ccag_hlaseni_komunikace.poradove_cislo IS 'Rok hlášení';


--
-- TOC entry 2215 (class 0 OID 0)
-- Dependencies: 188
-- Name: COLUMN ccag_hlaseni_komunikace.obsah_komunikace; Type: COMMENT; Schema: ccag; Owner: ccag
--

COMMENT ON COLUMN ccag_hlaseni_komunikace.obsah_komunikace IS 'Obsah komunikace';


--
-- TOC entry 2216 (class 0 OID 0)
-- Dependencies: 188
-- Name: COLUMN ccag_hlaseni_komunikace.id_zakaznika; Type: COMMENT; Schema: ccag; Owner: ccag
--

COMMENT ON COLUMN ccag_hlaseni_komunikace.id_zakaznika IS 'Identifikace osoby ze seznamu jmen';


--
-- TOC entry 2217 (class 0 OID 0)
-- Dependencies: 188
-- Name: COLUMN ccag_hlaseni_komunikace.datum_vlozeni; Type: COMMENT; Schema: ccag; Owner: ccag
--

COMMENT ON COLUMN ccag_hlaseni_komunikace.datum_vlozeni IS 'Datum a čas vložení, který vyplnil uživatel na formuláři HAP001F nebo HAP006F-';


--
-- TOC entry 2218 (class 0 OID 0)
-- Dependencies: 188
-- Name: COLUMN ccag_hlaseni_komunikace.id_komunikace; Type: COMMENT; Schema: ccag; Owner: ccag
--

COMMENT ON COLUMN ccag_hlaseni_komunikace.id_komunikace IS 'Identifikátor komunikace';


--
-- TOC entry 189 (class 1259 OID 17096)
-- Name: ccag_osoba; Type: TABLE; Schema: ccag; Owner: ccag
--

CREATE TABLE ccag_osoba (
    id_osoby numeric NOT NULL,
    jmeno character varying(50),
    prijmeni character varying(255) NOT NULL,
    id_firmy numeric,
	kod_osoby character varying(255)
);


ALTER TABLE ccag_osoba OWNER TO ccag;

--
-- TOC entry 2219 (class 0 OID 0)
-- Dependencies: 189
-- Name: TABLE ccag_osoba; Type: COMMENT; Schema: ccag; Owner: ccag
--

COMMENT ON TABLE ccag_osoba IS 'Osoby';


--
-- TOC entry 2220 (class 0 OID 0)
-- Dependencies: 189
-- Name: COLUMN ccag_osoba.id_osoby; Type: COMMENT; Schema: ccag; Owner: ccag
--

COMMENT ON COLUMN ccag_osoba.id_osoby IS 'Identifikátor osoby';


--
-- TOC entry 2221 (class 0 OID 0)
-- Dependencies: 189
-- Name: COLUMN ccag_osoba.jmeno; Type: COMMENT; Schema: ccag; Owner: ccag
--

COMMENT ON COLUMN ccag_osoba.jmeno IS 'Jméno osoby';


--
-- TOC entry 2222 (class 0 OID 0)
-- Dependencies: 189
-- Name: COLUMN ccag_osoba.prijmeni; Type: COMMENT; Schema: ccag; Owner: ccag
--

COMMENT ON COLUMN ccag_osoba.prijmeni IS 'Příjmení osoby';


--
-- TOC entry 2223 (class 0 OID 0)
-- Dependencies: 189
-- Name: COLUMN ccag_osoba.id_firmy; Type: COMMENT; Schema: ccag; Owner: ccag
--

COMMENT ON COLUMN ccag_osoba.id_firmy IS 'Identifikátor firmy, kam osoba patří';


--
-- TOC entry 190 (class 1259 OID 17102)
-- Name: ccag_systemy; Type: TABLE; Schema: ccag; Owner: ccag
--

CREATE TABLE ccag_systemy (
    id_systemu numeric NOT NULL,
    kod_systemu character varying(100) NOT NULL,
    poznamka character varying(2000),
    priznak_an_zivy character varying(1) DEFAULT 'T'::character varying NOT NULL
);


ALTER TABLE ccag_systemy OWNER TO ccag;

--
-- TOC entry 2224 (class 0 OID 0)
-- Dependencies: 190
-- Name: TABLE ccag_systemy; Type: COMMENT; Schema: ccag; Owner: ccag
--

COMMENT ON TABLE ccag_systemy IS 'Evidence zákaznických systémů';


--
-- TOC entry 2225 (class 0 OID 0)
-- Dependencies: 190
-- Name: COLUMN ccag_systemy.id_systemu; Type: COMMENT; Schema: ccag; Owner: ccag
--

COMMENT ON COLUMN ccag_systemy.id_systemu IS 'Jednoznačný identifikátor systému.';


--
-- TOC entry 2226 (class 0 OID 0)
-- Dependencies: 190
-- Name: COLUMN ccag_systemy.kod_systemu; Type: COMMENT; Schema: ccag; Owner: ccag
--

COMMENT ON COLUMN ccag_systemy.kod_systemu IS 'Jednoznačný kód systému.';


--
-- TOC entry 2227 (class 0 OID 0)
-- Dependencies: 190
-- Name: COLUMN ccag_systemy.poznamka; Type: COMMENT; Schema: ccag; Owner: ccag
--

COMMENT ON COLUMN ccag_systemy.poznamka IS 'Poznámky k systému';


--
-- TOC entry 2228 (class 0 OID 0)
-- Dependencies: 190
-- Name: COLUMN ccag_systemy.priznak_an_zivy; Type: COMMENT; Schema: ccag; Owner: ccag
--

COMMENT ON COLUMN ccag_systemy.priznak_an_zivy IS 'Příznak udává, jestli se se systémem pracuje.';


--
-- TOC entry 191 (class 1259 OID 25180)
-- Name: ccag_ukol; Type: TABLE; Schema: ccag; Owner: ccag
--

CREATE TABLE ccag_ukol (
    id_ukol numeric NOT NULL,
    popis text,
    resitel character varying(3),
    termin_splneni date,
    datum_splneni date,
    je_prijat boolean,
    id_dohlizejici numeric,
	id_resitel numeric
);


ALTER TABLE ccag_ukol OWNER TO ccag;

--
-- TOC entry 2229 (class 0 OID 0)
-- Dependencies: 191
-- Name: COLUMN ccag_ukol.popis; Type: COMMENT; Schema: ccag; Owner: ccag
--

COMMENT ON COLUMN ccag_ukol.popis IS 'Popis ukolu';


--
-- TOC entry 2230 (class 0 OID 0)
-- Dependencies: 191
-- Name: COLUMN ccag_ukol.resitel; Type: COMMENT; Schema: ccag; Owner: ccag
--

COMMENT ON COLUMN ccag_ukol.resitel IS 'Kdo ma ukol resit';


--
-- TOC entry 2231 (class 0 OID 0)
-- Dependencies: 191
-- Name: COLUMN ccag_ukol.termin_splneni; Type: COMMENT; Schema: ccag; Owner: ccag
--

COMMENT ON COLUMN ccag_ukol.termin_splneni IS 'Predpokladany termin splneni';


--
-- TOC entry 2232 (class 0 OID 0)
-- Dependencies: 191
-- Name: COLUMN ccag_ukol.datum_splneni; Type: COMMENT; Schema: ccag; Owner: ccag
--

COMMENT ON COLUMN ccag_ukol.datum_splneni IS 'Skutecne datum splneni';


--
-- TOC entry 2233 (class 0 OID 0)
-- Dependencies: 191
-- Name: COLUMN ccag_ukol.je_prijat; Type: COMMENT; Schema: ccag; Owner: ccag
--

COMMENT ON COLUMN ccag_ukol.je_prijat IS 'Resitel ukol prijal/neprijal';


--
-- TOC entry 2234 (class 0 OID 0)
-- Dependencies: 191
-- Name: COLUMN ccag_ukol.id_dohlizejici; Type: COMMENT; Schema: ccag; Owner: ccag
--

COMMENT ON COLUMN ccag_ukol.id_dohlizejici IS 'Kdo se ma o splneni dozvedet';


--
-- TOC entry 2171 (class 0 OID 17078)
-- Dependencies: 186
-- Data for Name: ccag_firma; Type: TABLE DATA; Schema: ccag; Owner: ccag
--

COPY ccag_firma (id_firmy, nazev) FROM stdin;
1	CCA
2	ZCU
\.


--
-- TOC entry 2172 (class 0 OID 17084)
-- Dependencies: 187
-- Data for Name: ccag_hlaseni; Type: TABLE DATA; Schema: ccag; Owner: ccag
--

COPY ccag_hlaseni (id_hlaseni, cislo_hlaseni, rok_hlaseni, druh_hlaseni, datum_vzniku, id_firmy, id_zakaznika, termin, pracnost, text_hlaseni, text_vyrizeni, kod_uzivatele_resi, kod_uzivatele_vyridil, datum_vyrizeni_hlaseni, kodpra_i, datum_i, kodpra_u, datum_u, kod_uzivatele_garant, kod_uzivatele_vyridil_garant, datum_vyrizeni_hlaseni_garant, priorita, nazev, id_systemu) FROM stdin;
1	1	2016	POZADAVEK	2016-01-01	2	3	2016-10-31	\N	Úpravy objednávek a smluv (registr smluv).Pøidat pole Èástky bez DPH. Èástku pøenášet do exportu registru smluv. Ve smlouvách upravit i import a formuláø FLP009F - Import smlouvy.	\N	SPI	\N	\N	SPI	2016-10-05	\N	\N	\N	\N	\N	1	Úpravy objednávek a smluv pro ISRS	2
2	1	2017	POZADAVEK	2017-11-25	2	3	2016-10-31	\N	Testovaci hlaseni pro ISZA zcu mobilni aplikaci.	\N	SPI	\N	\N	SPI	2016-10-05	\N	\N	\N	\N	\N	1	Úpravy objednávek a smluv pro ISRS	2
3	2	2017	VADA	2016-01-01	2	3	2016-10-31	\N	Oprava nahlasenych chyb v systemu.	\N	SPI	\N	\N	SPI	2016-10-05	\N	\N	\N	\N	\N	1	Úpravy objednávek a smluv pro ISRS	2
4	2	2016	POZADAVEK	2016-01-01	2	3	2016-10-31	\N	Oprava nahlasenych chyb v systemu.	\N	SPI	\N	\N	SPI	2016-10-05	\N	\N	\N	\N	\N	1	Úpravy objednávek a smluv pro ISRS	2
5	3	2017	DOTAZ	2016-01-01	2	3	2016-10-31	\N	Odhad pracnosti noveho projektu ISZA zcu mobilni aplikace.	\N	SPI	\N	\N	SPI	2016-10-05	\N	\N	\N	\N	\N	1	Úpravy objednávek a smluv pro ISRS	2
\.


--
-- TOC entry 2173 (class 0 OID 17090)
-- Dependencies: 188
-- Data for Name: ccag_hlaseni_komunikace; Type: TABLE DATA; Schema: ccag; Owner: ccag
--

COPY ccag_hlaseni_komunikace (id_hlaseni, poradove_cislo, obsah_komunikace, id_zakaznika, datum_vlozeni, id_komunikace, id_puvodce_komunikace) FROM stdin;
1	1	Úpravy objednávek a smluv (registr smluv).Přidat pole Částky bez DPH. Částku přenášet do exportu registru smluv. Ve smlouvách upravit i import a formulář FLP009F - Import smlouvy.	3	2016-10-05	1	1
1	2	CCA připravuje návrh 22-2016 v1.0.	3	2016-10-05	2	2
1	3	Návrh 22-2016 v1.0 zaslán zákazníkovi.	3	2016-10-05	3	6
\.


--
-- TOC entry 2174 (class 0 OID 17096)
-- Dependencies: 189
-- Data for Name: ccag_osoba; Type: TABLE DATA; Schema: ccag; Owner: ccag
--

COPY ccag_osoba (id_osoby, jmeno, prijmeni, id_firmy, kod_osoby) FROM stdin;
1	Petr	Přibyl	1	PPRIBYL
2	Jan	Novák	1	JNOVAK
3	Jan	Kebrle	2	JKEBRLE
4	Miroslav	Liška	2	MLISKA
5	Roman	Zeleník	2	RZELENIK
6 	Uzivatel	Uzivatel	2	USER
\.


--
-- TOC entry 2175 (class 0 OID 17102)
-- Dependencies: 190
-- Data for Name: ccag_systemy; Type: TABLE DATA; Schema: ccag; Owner: ccag
--

COPY ccag_systemy (id_systemu, kod_systemu, poznamka, priznak_an_zivy) FROM stdin;
1	ISZA	Informační systém zákaznické administrace	A
2	EKON	Ekonomika vysokých škol	A
\.


--
-- TOC entry 2176 (class 0 OID 25180)
-- Dependencies: 191
-- Data for Name: ccag_ukol; Type: TABLE DATA; Schema: ccag; Owner: ccag
--

COPY ccag_ukol (id_ukol, popis, resitel, termin_splneni, datum_splneni, je_prijat, id_dohlizejici) FROM stdin;
\.


--
-- TOC entry 2028 (class 2606 OID 17110)
-- Name: ccag_firma FIRMA_PK; Type: CONSTRAINT; Schema: ccag; Owner: ccag
--

ALTER TABLE ONLY ccag_firma
    ADD CONSTRAINT "FIRMA_PK" PRIMARY KEY (id_firmy);


--
-- TOC entry 2030 (class 2606 OID 17112)
-- Name: ccag_hlaseni HLASENI_PK; Type: CONSTRAINT; Schema: ccag; Owner: ccag
--

ALTER TABLE ONLY ccag_hlaseni
    ADD CONSTRAINT "HLASENI_PK" PRIMARY KEY (id_hlaseni);


--
-- TOC entry 2032 (class 2606 OID 17114)
-- Name: ccag_hlaseni HLASENI_UK; Type: CONSTRAINT; Schema: ccag; Owner: ccag
--

ALTER TABLE ONLY ccag_hlaseni
    ADD CONSTRAINT "HLASENI_UK" UNIQUE (rok_hlaseni, cislo_hlaseni);


--
-- TOC entry 2037 (class 2606 OID 17116)
-- Name: ccag_hlaseni_komunikace KOMUNIKACE_PK; Type: CONSTRAINT; Schema: ccag; Owner: ccag
--

ALTER TABLE ONLY ccag_hlaseni_komunikace
    ADD CONSTRAINT "KOMUNIKACE_PK" PRIMARY KEY (id_komunikace);


--
-- TOC entry 2040 (class 2606 OID 17118)
-- Name: ccag_osoba OSOBA_PK; Type: CONSTRAINT; Schema: ccag; Owner: ccag
--

ALTER TABLE ONLY ccag_osoba
    ADD CONSTRAINT "OSOBA_PK" PRIMARY KEY (id_osoby);


--
-- TOC entry 2043 (class 2606 OID 17120)
-- Name: ccag_systemy SYSTEM_PK; Type: CONSTRAINT; Schema: ccag; Owner: ccag
--

ALTER TABLE ONLY ccag_systemy
    ADD CONSTRAINT "SYSTEM_PK" PRIMARY KEY (id_systemu);


--
-- TOC entry 2045 (class 2606 OID 17122)
-- Name: ccag_systemy SYSTEM_UK; Type: CONSTRAINT; Schema: ccag; Owner: ccag
--

ALTER TABLE ONLY ccag_systemy
    ADD CONSTRAINT "SYSTEM_UK" UNIQUE (kod_systemu);


--
-- TOC entry 2047 (class 2606 OID 25187)
-- Name: ccag_ukol UKOL_PK; Type: CONSTRAINT; Schema: ccag; Owner: ccag
--

ALTER TABLE ONLY ccag_ukol
    ADD CONSTRAINT "UKOL_PK" PRIMARY KEY (id_ukol);


--
-- TOC entry 2033 (class 1259 OID 17123)
-- Name: fki_HLASENI_FIRMA_FK; Type: INDEX; Schema: ccag; Owner: ccag
--

CREATE INDEX "fki_HLASENI_FIRMA_FK" ON ccag_hlaseni USING btree (id_firmy);


--
-- TOC entry 2034 (class 1259 OID 17124)
-- Name: fki_HLASENI_OSOBA_FK; Type: INDEX; Schema: ccag; Owner: ccag
--

CREATE INDEX "fki_HLASENI_OSOBA_FK" ON ccag_hlaseni USING btree (id_zakaznika);


--
-- TOC entry 2035 (class 1259 OID 17125)
-- Name: fki_HLASENI_SYSTEM_FK; Type: INDEX; Schema: ccag; Owner: ccag
--

CREATE INDEX "fki_HLASENI_SYSTEM_FK" ON ccag_hlaseni USING btree (id_systemu);


--
-- TOC entry 2038 (class 1259 OID 17126)
-- Name: fki_KOMUNIKACE_HLASENI_FK; Type: INDEX; Schema: ccag; Owner: ccag
--

CREATE INDEX "fki_KOMUNIKACE_HLASENI_FK" ON ccag_hlaseni_komunikace USING btree (id_hlaseni);


--
-- TOC entry 2041 (class 1259 OID 17127)
-- Name: fki_OSOBA_FIRMA_FK; Type: INDEX; Schema: ccag; Owner: ccag
--

CREATE INDEX "fki_OSOBA_FIRMA_FK" ON ccag_osoba USING btree (id_firmy);


--
-- TOC entry 2048 (class 2606 OID 17128)
-- Name: ccag_hlaseni HLASENI_FIRMA_FK; Type: FK CONSTRAINT; Schema: ccag; Owner: ccag
--

ALTER TABLE ONLY ccag_hlaseni
    ADD CONSTRAINT "HLASENI_FIRMA_FK" FOREIGN KEY (id_firmy) REFERENCES ccag_firma(id_firmy);


--
-- TOC entry 2049 (class 2606 OID 17133)
-- Name: ccag_hlaseni HLASENI_OSOBA_FK; Type: FK CONSTRAINT; Schema: ccag; Owner: ccag
--

ALTER TABLE ONLY ccag_hlaseni
    ADD CONSTRAINT "HLASENI_OSOBA_FK" FOREIGN KEY (id_zakaznika) REFERENCES ccag_osoba(id_osoby);


--
-- TOC entry 2050 (class 2606 OID 17138)
-- Name: ccag_hlaseni HLASENI_SYSTEM_FK; Type: FK CONSTRAINT; Schema: ccag; Owner: ccag
--

ALTER TABLE ONLY ccag_hlaseni
    ADD CONSTRAINT "HLASENI_SYSTEM_FK" FOREIGN KEY (id_systemu) REFERENCES ccag_systemy(id_systemu);


--
-- TOC entry 2051 (class 2606 OID 17143)
-- Name: ccag_hlaseni_komunikace KOMUNIKACE_HLASENI_FK; Type: FK CONSTRAINT; Schema: ccag; Owner: ccag
--

ALTER TABLE ONLY ccag_hlaseni_komunikace
    ADD CONSTRAINT "KOMUNIKACE_HLASENI_FK" FOREIGN KEY (id_hlaseni) REFERENCES ccag_hlaseni(id_hlaseni);


--
-- TOC entry 2052 (class 2606 OID 17148)
-- Name: ccag_osoba OSOBA_FIRMA_FK; Type: FK CONSTRAINT; Schema: ccag; Owner: ccag
--

ALTER TABLE ONLY ccag_osoba
    ADD CONSTRAINT "OSOBA_FIRMA_FK" FOREIGN KEY (id_firmy) REFERENCES ccag_firma(id_firmy);


--
-- TOC entry 2053 (class 2606 OID 25188)
-- Name: ccag_ukol fki_UKOL_OSOBA; Type: FK CONSTRAINT; Schema: ccag; Owner: ccag
--

ALTER TABLE ONLY ccag_ukol
    ADD CONSTRAINT "fki_UKOL_OSOBA" FOREIGN KEY (id_dohlizejici) REFERENCES ccag_osoba(id_osoby);

ALTER TABLE ONLY ccag_ukol
    ADD CONSTRAINT "fki_UKOL_OSOBA" FOREIGN KEY (id_resitel) REFERENCES ccag_osoba(id_osoby);

--
-- TOC entry 2182 (class 0 OID 0)
-- Dependencies: 7
-- Name: ccag; Type: ACL; Schema: -; Owner: ccag
--

GRANT USAGE ON SCHEMA ccag TO ccag_apl;

ALTER TABLE ccag.ccag_osoba ADD COLUMN je_zamestnanec boolean;
UPDATE ccag.ccag_osoba SET je_zamestnanec = true;

-- Completed on 2017-10-17 12:09:12

--
-- PostgreSQL database dump complete
--
