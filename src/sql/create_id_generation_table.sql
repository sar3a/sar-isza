DROP TABLE IF EXISTS ccag.ccag_pk_id_generator;

CREATE TABLE ccag.ccag_pk_id_generator(
table_name character varying(255) PRIMARY KEY UNIQUE NOT NULL,
next_id numeric NOT NULL
);

ALTER TABLE ccag.ccag_pk_id_generator OWNER TO ccag;

INSERT INTO ccag.ccag_pk_id_generator VALUES 
('ccag_hlaseni_komunikace',1),
('ccag_ukol',1);

UPDATE ccag.ccag_pk_id_generator 
SET next_id = (SELECT MAX(id_komunikace)+1 FROM ccag.ccag_hlaseni_komunikace)
WHERE table_name='ccag_hlaseni_komunikace' 
AND ((SELECT MAX(id_komunikace) FROM ccag.ccag_hlaseni_komunikace) IS NOT NULL);

UPDATE ccag.ccag_pk_id_generator 
SET next_id = (SELECT MAX(id_ukol)+1 FROM ccag.ccag_ukol)
WHERE table_name='ccag_hlaseni_komunikace' 
AND ((SELECT MAX(id_ukol) FROM ccag.ccag_ukol) IS NOT NULL);

