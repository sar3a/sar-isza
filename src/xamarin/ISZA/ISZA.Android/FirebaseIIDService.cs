﻿using System;
using Android.App;
using Firebase.Iid;
using Android.Util;

namespace ISZA.Droid
{
    [Service]
    [IntentFilter(new[] { "com.google.firebase.INSTANCE_ID_EVENT" })]
    public class FirebaseIIDService : FirebaseInstanceIdService
    {
        const string TAG = "FirebaseIIDService";
        public override void OnTokenRefresh()
        {
            Log.Debug(TAG, "Refreshed token: " + FirebaseInstanceId.Instance.Token);
            App.signer.SetFirebaseToken(FirebaseInstanceId.Instance.Token);
        }
    }
}