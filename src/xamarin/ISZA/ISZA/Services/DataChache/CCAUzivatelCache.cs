﻿using ISZA.Models;
using ISZA.Services.DataWorker.Uzivatel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ISZA.Services.DataChache
{
    public class CCAUzivatelCache
    {
        private IEnumerable<CCAUzivatel> uzivateleList;
        private IUzivatelDataWorker _iudw => DependencyService.Get<IUzivatelDataWorker>() ?? new UzivatelDataWorker();


        public CCAUzivatelCache()
        {
            uzivateleList = new List<CCAUzivatel>();
        }

        public async Task NactiUzivatele()
        {
            this.uzivateleList = await _iudw.GetSeznamUzivatelu();
        }

        public IEnumerable<CCAUzivatel> GetCCAUzivatelList()
        {
            return this.uzivateleList;
        }

        public IList<CCAUzivatel> GetCCAZamestnanecList()
        {
            // Pro testovani
            //var uzivateleList = new List<CCAUzivatel>(this.uzivateleList);
            //uzivateleList.First().JeZamestnanec = false;
            
            return uzivateleList.Where(uzivatel => uzivatel.JeZamestnanec).ToList();
        }
    }
}