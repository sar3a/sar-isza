﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISZA.Services.DataWorker
{
    /// <summary>
    /// Pro komunikace se serverem pak provolava IDataStore
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IDataWorker<T>
    {
        Task<bool> PridejAsync(T item);
        Task<bool> AktualizujAsync(T item);
        Task<bool> SmazAsync(int id);
        Task<T> GetItemAsync(int id);
        Task<IEnumerable<T>> GetItemsAsync(bool forceRefresh = false);
    }
}
