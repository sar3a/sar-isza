﻿using ISZA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISZA.Services.DataWorker.Komunikace
{
    public interface IKomunikaceDataWorker: IDataWorker<CCAKomunikace>
    {
        //Task<IEnumerable<CCAKomunikace>> GetKomunikaceUzivatele(string kodUzivatele);
        Task<IEnumerable<CCAKomunikace>> GetKomunikaceHlaseni(int idHlaseni);

    }
}
