﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ISZA.Models;
using ISZA.Services.DataStore.Komunikace;
using Xamarin.Forms;
using ISZA.Models.Rest;

namespace ISZA.Services.DataWorker.Komunikace
{
    public class KomunikaceDataWorker : IKomunikaceDataWorker
    {
        private IKomunikaceDataStore _idrs => DependencyService.Get<IKomunikaceDataStore>() ?? new KomunikaceDataStore();

        public Task<bool> AktualizujAsync(CCAKomunikace item)
        {
            throw new NotImplementedException();
        }

        public Task<CCAKomunikace> GetItemAsync(int id)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<CCAKomunikace>> GetItemsAsync(bool forceRefresh = false)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<CCAKomunikace>> GetKomunikaceHlaseni(int idHlaseni)
        {
            var items = await _idrs.GetKomunikaceHlaseni(idHlaseni);
            return items.Select(x => new CCAKomunikace()
            {
                Id = x.idKomunikace,
                CCAHlaseniId = x.idHlaseni.ToString(),
                Obsah = x.obsahKomunikace,
                DatumVlozeni = x.datumVlozeni,
                IdPuvodce = x.idPuvodceKomunikace,
            }).OrderBy(x => x.DatumVlozeni).AsEnumerable();
        }

        //public async Task<IEnumerable<CCAKomunikace>> GetKomunikaceUzivatele(string kodUzivatele)
        //{
        //    var items = await _idrs.GetKomunikaceUzivatele(kodUzivatele);
        //    return items.Select(x => new CCAKomunikace()
        //    {
        //        Id = x.idKomunikace,
        //        CCAHlaseniId = x.idHlaseni.ToString(),
        //        Obsah = x.obsahKomunikace,
        //        DatumVlozeni = x.datumVlozeni,
        //        IdZakaznika = x.idPuvodceKomunikace.ToString(),
        //    }).OrderBy(x => x.DatumVlozeni).AsEnumerable();
        //}

        public Task<bool> PridejAsync(CCAKomunikace item)
        {
            try
            {
                KomunikaceDto toAdd = new KomunikaceDto()
                {
                    datumVlozeni = item.DatumVlozeni,
                    idHlaseni = int.Parse(item.CCAHlaseniId),
                    idPuvodceKomunikace = item.IdPuvodce,
                    obsahKomunikace = item.Obsah,
                };
                return _idrs.PridejAsync(toAdd);
            }
            catch (Exception ex)
            {
                if (ex is ArgumentNullException || ex is FormatException || ex is OverflowException)
                {
                    //Chyba pri prevodu, zceknout co se deje, ale nastavat by nemela
                    Debug.WriteLine(ex);
                    return Task.FromResult(false);
                }
                return Task.FromResult(false);
            }
        }

        public Task<bool> SmazAsync(int id)
        {
            throw new NotImplementedException();
        }
    }
}
