﻿using ISZA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISZA.Services.DataWorker.Uzivatel
{
    public interface IUzivatelDataWorker
    {
        Task<CCAUzivatel> GetUzivatele(string username);
        Task<IEnumerable<CCAUzivatel>> GetSeznamUzivatelu();
        Task<bool> NastavTokenUzivatele(CCAUzivatel uzivatel);
    }
}
