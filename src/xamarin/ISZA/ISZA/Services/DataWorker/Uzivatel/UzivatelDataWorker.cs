﻿using ISZA.Models;
using ISZA.Models.Rest;
using ISZA.Services.DataStore.Uzivatel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ISZA.Services.DataWorker.Uzivatel
{
    public class UzivatelDataWorker : IUzivatelDataWorker
    {
        private IUzivatelDataStore _iuds => DependencyService.Get<IUzivatelDataStore>() ?? new UzivatelDataStore();


        public async Task<CCAUzivatel> GetUzivatele(string username)
        {
            var uzivatel = await _iuds.GetUzivatele(username);
            if (uzivatel != null)
            {
                return new CCAUzivatel() { Id = uzivatel.idOsoby, Jmeno = uzivatel.jmeno, Prijmeni = uzivatel.prijmeni, KodOsoby = uzivatel.kodOsoby, JeZamestnanec = uzivatel.jeZamestnanec, FirebaseToken = uzivatel.firebaseToken};
            }
            return null;
        }

        public async Task<IEnumerable<CCAUzivatel>> GetSeznamUzivatelu()
        {
            var uzivatele = await _iuds.GetSeznamUzivatelu();
            return uzivatele.Select(x => new CCAUzivatel()
            {
                Id = x.idOsoby,
                Jmeno = x.jmeno,
                Prijmeni = x.prijmeni,
                KodOsoby = x.kodOsoby,
                JeZamestnanec = x.jeZamestnanec,
                FirebaseToken = x.firebaseToken
            }).AsEnumerable();
        }

        public async Task<bool> NastavTokenUzivatele(CCAUzivatel uzivatel)
        {
            //Vic toho nemapuju, neni potreba
            var toSend = new OsobaDto()
            {
                idOsoby = uzivatel.Id,
                firebaseToken = uzivatel.FirebaseToken
            };
            var result = await _iuds.NastavTokenUzivatele(toSend);
            return result;
        }
    }
}
