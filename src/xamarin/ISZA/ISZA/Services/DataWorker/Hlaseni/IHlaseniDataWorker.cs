﻿using ISZA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISZA.Services.DataWorker.Hlaseni
{
    public interface IHlaseniDataWorker : IDataWorker<CCAHlaseni>
    {
        Task<IEnumerable<CCAHlaseni>> getOtevrenaHlaseni();
    }
}
