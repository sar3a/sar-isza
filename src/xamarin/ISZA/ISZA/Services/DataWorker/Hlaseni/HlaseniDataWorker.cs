﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ISZA.Models;
using ISZA.Services.DataStore.Hlaseni;
using Xamarin.Forms;

namespace ISZA.Services.DataWorker.Hlaseni
{
    public class HlaseniDataWorker : IHlaseniDataWorker
    {
        private IHlaseniDataStore _ihd => DependencyService.Get<IHlaseniDataStore>() ?? new HlaseniDataStore();

        public Task<bool> AktualizujAsync(CCAHlaseni item)
        {
            throw new NotImplementedException();
        }

        public async Task<CCAHlaseni> GetItemAsync(int id)
        {
            var toConvert = await _ihd.GetItemAsync(id);
            return new CCAHlaseni()
            {
                Cislo = toConvert.cisloHlaseni.ToString(),
                Druh = toConvert.druhHlaseni,
                Nazev = toConvert.nazev,
                Obsah = toConvert.textHlaseni,
                Rok = toConvert.rokHlaseni.ToString(),
                System = toConvert.system.kodSystemu,
                IdHlaseni = toConvert.idHlaseni.ToString()
            };
        }

        public Task<IEnumerable<CCAHlaseni>> GetItemsAsync(bool forceRefresh = false)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<CCAHlaseni>> getOtevrenaHlaseni()
        {
            var toConvert = await _ihd.getOtevrenaHlaseni();
            return toConvert.Select(x => new CCAHlaseni()
            {
                Cislo = x.cisloHlaseni.ToString(),
                Druh = x.druhHlaseni,
                IdHlaseni = x.idHlaseni.ToString(),
                Nazev = x.nazev,
                Obsah = x.textHlaseni,
                Rok = x.rokHlaseni.ToString(),
                System = x.system.kodSystemu
            }).AsEnumerable();

        }

        public Task<bool> PridejAsync(CCAHlaseni item)
        {
            throw new NotImplementedException();
        }

        public Task<bool> SmazAsync(int id)
        {
            throw new NotImplementedException();
        }
    }
}
