﻿using ISZA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISZA.Services.DataWorker.Ukol
{
    public interface IUkolDataWorker: IDataWorker<CCAUkol>
    {
        Task<IEnumerable<CCAUkol>> GetUkolyZadaneUzivateli(int idUzivatele);
        Task<IEnumerable<CCAUkol>> GetUkolyDohlizeneUzivatelem(int idUzivatele);
        Task<bool> NastavStavUkolu(CCAUkol ukol);
    }
}
