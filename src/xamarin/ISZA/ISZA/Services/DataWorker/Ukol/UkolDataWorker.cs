﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ISZA.Models;
using ISZA.Services.DataStore.Ukol;
using Xamarin.Forms;
using ISZA.Models.Rest;

namespace ISZA.Services.DataWorker.Ukol
{
    public class UkolDataWorker : IUkolDataWorker
    {
        private IUkolDataStore _iuds => DependencyService.Get<IUkolDataStore>() ?? new UkolDataStore();

        public Task<bool> AktualizujAsync(CCAUkol item)
        {
            throw new NotImplementedException();
        }

        public Task<CCAUkol> GetItemAsync(int id)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<CCAUkol>> GetItemsAsync(bool forceRefresh = false)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<CCAUkol>> GetUkolyDohlizeneUzivatelem(int idUzivatele)
        {
            var result = await _iuds.GetUkolyDohlizeneUzivatelem(idUzivatele);
            return result.Select(x => new CCAUkol() { IdResitele = x.idResitel, IdUkolu = x.idUkolu.ToString(), IdZadavatele = x.idDohlizejici, JePrijat = x.jePrijat, Popis = x.popis, TerminSplneni = x.terminSplneni});
        }

        public async Task<IEnumerable<CCAUkol>> GetUkolyZadaneUzivateli(int idUzivatele)
        {
            var result = await _iuds.GetUkolyZadaneUzivateli(idUzivatele);
            return result.Select(x => new CCAUkol() { IdResitele = x.idResitel, IdUkolu = x.idUkolu.ToString(), IdZadavatele = x.idDohlizejici, JePrijat = x.jePrijat, Popis = x.popis, TerminSplneni = x.terminSplneni });

        }

        public Task<bool> NastavStavUkolu(CCAUkol ukol)
        {

            return _iuds.NastavStavUkolu(new UkolDto() {idUkolu = int.Parse(ukol.IdUkolu),jePrijat = ukol.JePrijat});
        }

        public Task<bool> PridejAsync(CCAUkol item)
        {
            UkolDto toAdd = new UkolDto() { idDohlizejici = item.IdZadavatele, idResitel = item.IdResitele, terminSplneni = item.TerminSplneni, jePrijat = false, popis = item.Popis };
            return _iuds.PridejAsync(toAdd);
        }

        public Task<bool> SmazAsync(int id)
        {
            throw new NotImplementedException();
        }
    }
}
