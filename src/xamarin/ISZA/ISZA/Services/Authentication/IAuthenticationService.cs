﻿using ISZA.Models;

namespace ISZA.Services.Authentication
{
    public interface IAuthenticationService
    {

        bool Authenticate(CCAUzivatel user);
        string GetUsername();
        int GetIdUzivatele();
    }
}