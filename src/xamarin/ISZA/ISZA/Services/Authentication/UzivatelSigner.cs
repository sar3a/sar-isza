﻿using ISZA.Models;
using ISZA.Services.DataWorker.Uzivatel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ISZA.Services.Authentication
{
    public class UzivatelSigner
    {

        private IUzivatelDataWorker _iudw => DependencyService.Get<IUzivatelDataWorker>() ?? new UzivatelDataWorker();

        private CCAUzivatel uzivatel;

        public string FirebaseToken = null;

        public bool jePrihlaseny { get; set; }

        public void SetFirebaseToken(string token)
        {
            if(token != null)
            {
                FirebaseToken = token;
            }
        }

        public UzivatelSigner()
        {
            jePrihlaseny = false;
            uzivatel = null;
            //Tady by slo udelat nacitani ze souboru z defaultniho mista, pokud tam nic neni
        }

        public void SetUzivatel(CCAUzivatel uzivatel)
        {
            this.uzivatel = uzivatel;
        }

        public CCAUzivatel GetUzivatel()
        {
            return this.uzivatel;
        }

        public async Task<bool> PrihlasMe(string zkratkaUzivatele)
        {
            var uzivatel = await _iudw.GetUzivatele(zkratkaUzivatele);
            if (uzivatel != null)
            {
                this.jePrihlaseny = true;
                this.uzivatel = uzivatel;
                if(this.uzivatel.FirebaseToken != this.FirebaseToken && this.FirebaseToken != null)
                {
                    this.uzivatel.FirebaseToken = this.FirebaseToken;
                    _iudw.NastavTokenUzivatele(uzivatel);
                }
            }
            else
            {
                this.jePrihlaseny = false;
                this.uzivatel = null;
            }
            return await Task.FromResult<bool>(this.jePrihlaseny);
        }
    }
}
