﻿using ISZA.Models;
using ISZA.Services.Authentication;
using ISZA.Services.DataWorker.Uzivatel;
using Xamarin.Forms;

// registration for dependency injection
[assembly: Dependency(typeof(LoginService))]

namespace ISZA.Services.Authentication
{
    public class LoginService : IAuthenticationService
    {
        private IUzivatelDataWorker _iudw => DependencyService.Get<IUzivatelDataWorker>() ?? new UzivatelDataWorker();

        public LoginService()
        {
        }

        public bool Authenticate(CCAUzivatel user)
        {
            var uzivatel = _iudw.GetUzivatele(user.KodOsoby);
            if (uzivatel != null)
            {
                App.IsUserLoggedIn = true;
            }
            return user.Jmeno == "User";
        }

        public string GetUsername()
        {
            if (App.signer.GetUzivatel() != null)
            {
                return App.signer.GetUzivatel().KodOsoby;
            }
            else
            {
                return "";
            }
        }

        public int GetIdUzivatele()
        {
            if (App.signer.GetUzivatel() != null)
            {
                return App.signer.GetUzivatel().Id;
            }
            else
            {
                return -1;
            }
        }
    }
}