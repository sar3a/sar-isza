﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ISZA.Services.DataStore
{
    /// <summary>
    /// This is copy of Sample.IDataStore&lt;
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IDataStore<T>
    {
        Task<bool> PridejAsync(T item);
        Task<bool> AktualizujAsync(T item);
        Task<bool> SmazAsync(int id);
        Task<T> GetItemAsync(int id);
        Task<IEnumerable<T>> GetItemsAsync(bool forceRefresh = false);
    }
}