﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ISZA.Models.Rest;
using ISZA.Services.Rest;
using Newtonsoft.Json;
using ISZA.Services.Authentication;
using Xamarin.Forms;
using ISZA.Services.DataStore.Komunikace;

[assembly: Dependency(typeof(KomunikaceDataStore))]

namespace ISZA.Services.DataStore.Komunikace
{
    public class KomunikaceDataStore: IKomunikaceDataStore
    {
        private IRestService _irs => DependencyService.Get<IRestService>() ?? new RestService();
        private IAuthenticationService _ias => DependencyService.Get<IAuthenticationService>() ?? new LoginService();


        public Task<bool> PridejAsync(KomunikaceDto item)
        {
            var result = _irs.Post<KomunikaceDto>("vytvorKomunikaci", JsonConvert.SerializeObject(item)).Result;
            if (result != null)
            {
                return Task.FromResult(true);
            }
            return Task.FromResult(false);
        }

        public Task<bool> AktualizujAsync(KomunikaceDto item)
        {
            throw new System.NotImplementedException();
        }

        public Task<bool> SmazAsync(int id)
        {
            throw new System.NotImplementedException();
        }

        public Task<KomunikaceDto> GetItemAsync(int id)
        {
            throw new System.NotImplementedException();
        }

        public Task<IEnumerable<KomunikaceDto>> GetItemsAsync(bool forceRefresh = false)
        {

            throw new System.NotImplementedException();
        }

        public Task<IEnumerable<KomunikaceDto>> GetKomunikaceUzivatele(string kodUzivatele)
        {
            return _irs.GetResponse<IEnumerable<KomunikaceDto>>("/komunikaceUzivatele/"+ kodUzivatele);
        }

        public Task<IEnumerable<KomunikaceDto>> GetKomunikaceHlaseni(int idHlaseni)
        {
            return _irs.GetResponse<IEnumerable<KomunikaceDto>>("/komunikaceHlaseni/" + idHlaseni);
        }
    }
}