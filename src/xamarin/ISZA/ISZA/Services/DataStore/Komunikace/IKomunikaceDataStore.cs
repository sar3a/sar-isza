﻿using ISZA.Models.Rest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISZA.Services.DataStore.Komunikace
{
    public interface IKomunikaceDataStore : IDataStore<KomunikaceDto>
    {
        Task<IEnumerable<KomunikaceDto>> GetKomunikaceUzivatele(string kodUzivatele);
        Task<IEnumerable<KomunikaceDto>> GetKomunikaceHlaseni(int idHlaseni);
    }
}
