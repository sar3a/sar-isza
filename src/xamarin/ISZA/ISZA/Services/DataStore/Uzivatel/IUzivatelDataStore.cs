﻿using ISZA.Models;
using ISZA.Models.Rest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISZA.Services.DataStore.Uzivatel
{
    public interface IUzivatelDataStore
    {
        Task<OsobaDto> GetUzivatele(string username);
        Task<IEnumerable<OsobaDto>> GetSeznamUzivatelu();
        Task<bool> NastavTokenUzivatele(OsobaDto uzivatel);

    }
}
