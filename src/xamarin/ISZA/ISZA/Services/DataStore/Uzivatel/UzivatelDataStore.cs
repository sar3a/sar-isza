﻿using ISZA.Services.DataStore.Uzivatel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ISZA.Models;
using ISZA.Services.Rest;
using Xamarin.Forms;
using ISZA.Models.Rest;
using Newtonsoft.Json;

namespace ISZA.Services.DataStore.Uzivatel
{
    public class UzivatelDataStore : IUzivatelDataStore
    {
        private IRestService _irs => DependencyService.Get<IRestService>() ?? new RestService();


        public Task<OsobaDto> GetUzivatele(string username)
        {
            return _irs.GetResponse<OsobaDto>("/prihlasUzivatele/" + username);
        }

        public Task<IEnumerable<OsobaDto>> GetSeznamUzivatelu()
        {
            return _irs.GetResponse<IEnumerable<OsobaDto>>("/seznamOsob/");

        }

        public Task<bool> NastavTokenUzivatele(OsobaDto uzivatel)
        {
            var result = _irs.Post<OsobaDto>("/nastavTokenUzivatele", JsonConvert.SerializeObject(uzivatel)).Result;
            if (result != null)
            {
                return Task.FromResult(true);
            }
            else
            {
                return Task.FromResult(false);
            }


        }

    }
}
