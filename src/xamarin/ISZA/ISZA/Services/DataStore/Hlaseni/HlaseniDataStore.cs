﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;
using ISZA.Services.Rest;
using ISZA.Models.Rest;
using ISZA.Services.DataStore.Hlaseni;

// registration for dependency injection
[assembly: Dependency(typeof(HlaseniDataStore))]

namespace ISZA.Services.DataStore.Hlaseni
{
    public class HlaseniDataStore : IHlaseniDataStore
    {
        private IRestService _irs => DependencyService.Get<IRestService>() ?? new RestService();

        public HlaseniDataStore()
        {
        }

        public Task<bool> PridejAsync(HlaseniDto item)
        {
            throw new System.NotImplementedException();
        }

        public Task<bool> SmazAsync(int id)
        {
            throw new System.NotImplementedException();
        }

        public async Task<HlaseniDto> GetItemAsync(int id)
        {
            var restResult = await Task.FromResult(_irs.GetResponse<List<HlaseniDto>>("/otevrenaHlaseni"));
            List<HlaseniDto> result = restResult.Result.ToList();
            return await Task.FromResult(result.FirstOrDefault(s => s.idHlaseni == id));
        }

        public Task<IEnumerable<HlaseniDto>> GetItemsAsync(bool forceRefresh = false)
        {
            throw new System.NotImplementedException();
        }

        public Task<bool> AktualizujAsync(HlaseniDto item)
        {
            throw new System.NotImplementedException();
        }

        public async Task<IEnumerable<HlaseniDto>> getOtevrenaHlaseni()
        {
            var restResult = await Task.FromResult(_irs.GetResponse<List<HlaseniDto>>("/otevrenaHlaseni"));
            return restResult.Result;
        }
    }
}