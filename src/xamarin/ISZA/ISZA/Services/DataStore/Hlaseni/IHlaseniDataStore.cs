﻿using ISZA.Models.Rest;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ISZA.Services.DataStore.Hlaseni
{
    public interface IHlaseniDataStore : IDataStore<HlaseniDto>
    {
        Task<IEnumerable<HlaseniDto>> getOtevrenaHlaseni();
    }
}
