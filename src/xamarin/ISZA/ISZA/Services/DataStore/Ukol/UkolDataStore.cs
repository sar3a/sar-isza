﻿using ISZA.Models.Rest;
using ISZA.Services.Authentication;
using ISZA.Services.DataStore.Ukol;
using ISZA.Services.Rest;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;

[assembly: Dependency(typeof(UkolDataStore))]

namespace ISZA.Services.DataStore.Ukol
{
    public class UkolDataStore : IUkolDataStore
    {

        private IRestService _irs => DependencyService.Get<IRestService>() ?? new RestService();
        private IAuthenticationService _ias => DependencyService.Get<IAuthenticationService>() ?? new LoginService();

        public Task<bool> PridejAsync(UkolDto item)
        {
            var result = _irs.Post<UkolDto>("/vytvorUkol", JsonConvert.SerializeObject(item)).Result;
            if (result != null)
            {
                return Task.FromResult(true);
            }
            return Task.FromResult(false);
        }

        public Task<bool> AktualizujAsync(UkolDto item)
        {
            throw new NotImplementedException();
        }

        public Task<bool> SmazAsync(int id)
        {
            throw new NotImplementedException();
        }

        public Task<UkolDto> GetItemAsync(int id)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<UkolDto>> GetItemsAsync(bool forceRefresh = false)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<UkolDto>> GetUkolyDohlizeneUzivatelem(int idUzivatele)
        {
            return _irs.GetResponse<IEnumerable<UkolDto>>("/ukolyDohlizeneUzivatelem/" + idUzivatele);
        }

        public Task<IEnumerable<UkolDto>> GetUkolyZadaneUzivateli(int idUzivatele)
        {
            return _irs.GetResponse<IEnumerable<UkolDto>>("/ukolyZadaneUzivateli/" + idUzivatele);
        }

        public Task<bool> NastavStavUkolu(UkolDto ukol)
        {
            var result = _irs.Post<UkolDto>("/nastavStavUkolu", JsonConvert.SerializeObject(ukol)).Result;
            if (result != null)
            {
                return Task.FromResult(true);
            }
            return Task.FromResult(false);
        }


    }
}
