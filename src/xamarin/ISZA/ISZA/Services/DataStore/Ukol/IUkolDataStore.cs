﻿using ISZA.Models.Rest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISZA.Services.DataStore.Ukol
{
    public interface IUkolDataStore : IDataStore<UkolDto>
    {
        Task<IEnumerable<UkolDto>> GetUkolyZadaneUzivateli(int idUzivatele);
        Task<IEnumerable<UkolDto>> GetUkolyDohlizeneUzivatelem(int idUzivatele);
        //V ukolu je atribut jePrijat
        Task<bool> NastavStavUkolu(UkolDto ukol);
    }
}
