﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISZA.Services.Rest
{
    public interface IRestService
    {
        Task<T> Post<T>(string webUrl, string jsonString) where T : class;
        Task<T> GetResponse<T>(string webUrl) where T : class;
    }
}
