﻿using ISZA.Services.Rest;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

// registration for dependency injection
[assembly: Dependency(typeof(RestService))]

namespace ISZA.Services.Rest
{
    public class RestService : IRestService
    {
        private HttpClient client;

        public RestService()
        {
            client = new HttpClient();
            client.MaxResponseContentBufferSize = 250000;
            client.DefaultRequestHeaders.Accept.Add(
                new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
        }

        /// <summary>
        /// Postne data ve formatu json na pozadovanou adresu
        /// Vrati zparsovany vysledek z formatu JSON
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="webUrl"></param>
        /// <param name="jsonString"></param>
        /// <returns></returns>
        public async Task<T> Post<T>(string webUrl, string jsonString) where T : class
        {
            var result = await client
                .PostAsync(App.RestApiUrl + webUrl, new StringContent(jsonString, Encoding.UTF8, "application/json"))
                .ConfigureAwait(false);
            var jsonResult = result.Content.ReadAsStringAsync().Result;
            var conentResp = JsonConvert.DeserializeObject<T>(jsonResult);
            return conentResp;
        }

        /// <summary>
        /// Posle get request na adresu z parametru. 
        /// Predpoklada odpoved ve formatu JSON
        /// JSON prevede na pozadovany objekt (genericky v T)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="webUrl"></param>
        /// <returns></returns>
        public async Task<T> GetResponse<T>(string webUrl) where T : class
        {
            var uri = new Uri(string.Format(App.RestApiUrl + webUrl, string.Empty));
            var response = await client.GetAsync(uri).ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<T>(content);
            }
            else
            {
                //TODO: handle errory
            }

            return null;
        }
    }
}