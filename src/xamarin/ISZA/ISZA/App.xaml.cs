﻿using ISZA.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ISZA.Services.Authentication;
using ISZA.Services.DataChache;
using System.Threading.Tasks;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]

namespace ISZA
{
    public partial class App : Application
    {
        public static string RestApiUrl = "http://kebrlej.westeurope.cloudapp.azure.com/";
        public static bool IsUserLoggedIn { get; set; }
        public readonly static UzivatelSigner signer = new UzivatelSigner();
        public readonly static CCAUzivatelCache uzivatelCache = new CCAUzivatelCache();

        public App()
        {
            InitializeComponent();
            Task.Run(() => uzivatelCache.NactiUzivatele()).Wait();
            Page mainPage;


            if (signer.jePrihlaseny)
            {
                mainPage = new MainPage();
            }
            else
            {
                mainPage = new LoginPage();
            }

            if (Device.RuntimePlatform == Device.iOS)
            {
                MainPage = mainPage;
            }
            else
            {
                MainPage = new NavigationPage(mainPage);
            }

        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}