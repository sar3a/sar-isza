﻿using System;
using System.Reflection;
using System.Text;
using Xamarin.Forms.Internals;

namespace ISZA.Models
{
    public class CCAUkol
    {
        public string IdUkolu { get; set; }
        public string Popis { get; set; }
        public bool JePrijat { get; set; }

        public int IdZadavatele { get; set; }

        //public string JmenoZadavatele { get; set; }
        public int IdResitele { get; set; }

        //public string JmenoResitele { get; set; }
        public DateTime TerminSplneni { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idUzivatele"></param>
        /// <returns>True pokud uzivatel s id <paramref name="idUzivatele"/> je resitelem tohoto ukolu</returns>
        public bool JeResitel(int idUzivatele)
        {
            return idUzivatele == IdResitele;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idUzivatele"></param>
        /// <returns>True pokud uzivatel s id <paramref name="idUzivatele"/> je zadavatelem tohoto ukolu</returns>
        public bool JeZadavatel(int idUzivatele)
        {
            return idUzivatele == IdZadavatele;
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder("CCAUkol{");
            foreach (PropertyInfo propertyInfo in GetType().GetProperties())
            {
                string name = propertyInfo.Name;
                object value = propertyInfo.GetValue(this);
                builder.AppendFormat("{0}={1} ", name, value);
            }

            builder.Append("}");
            return builder.ToString();
        }
    }
}