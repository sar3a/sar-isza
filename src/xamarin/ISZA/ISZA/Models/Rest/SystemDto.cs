﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISZA.Models.Rest
{
    public class SystemDto
    {
        public int idSystemu{get;set;}
        public string kodSystemu {get;set;}
        public string poznamka {get;set;}
        public string priznakAnZivy {get;set;}
    }
}
