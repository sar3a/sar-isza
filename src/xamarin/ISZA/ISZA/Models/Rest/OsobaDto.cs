﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISZA.Models.Rest
{
    public class OsobaDto
    {
        public int idOsoby { get; set; }
        public string jmeno { get; set; }
        public string prijmeni { get; set; }
        public FirmaDto firma { get; set; }
        public string kodOsoby { get; set; }
        public bool jeZamestnanec { get; set; }
        public string firebaseToken { get; set; }
    }
}
