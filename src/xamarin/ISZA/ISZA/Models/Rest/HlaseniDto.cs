﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISZA.Models.Rest
{
    public class HlaseniDto
    {
        public int idHlaseni { get; set; }
        public int cisloHlaseni { get; set; } // 10557, 11667,6050
        public int rokHlaseni { get; set; }  //2016, 2017
        public string druhHlaseni { get; set; } //napr. VADA, POZADAVEK, NAMET
        public DateTime? datumVyrizeniHlaseni { get; set; }
        public string nazev { get; set; } // napr.: Posun terminu - chyb. hlaska, Standardni zakazky, ISZA - garant modulu
        public string textHlaseni { get; set; }
        public string kodUzivateleResi { get; set; } // budeme potrebovat id resitele
        public SystemDto system { get; set; }

    }
}
