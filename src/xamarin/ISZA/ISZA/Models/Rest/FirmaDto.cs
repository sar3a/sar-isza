﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISZA.Models.Rest
{
    public class FirmaDto
    {
        public int idFirmy { get; set; }
        public string nazev { get; set; }
    }
}
