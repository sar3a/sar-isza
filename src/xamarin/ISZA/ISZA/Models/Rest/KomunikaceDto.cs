﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISZA.Models.Rest
{
    public class KomunikaceDto
    {
        public int idKomunikace { get; set; }
        //public int poradoveCislo {get;set;}
        public string obsahKomunikace {get;set;}
        //public string idZakaznika {get;set;}
        public int idPuvodceKomunikace { get; set; }
        public DateTime datumVlozeni {get;set;}
        public int idHlaseni {get;set;}

        //public override string ToString()
        //{
        //    StringBuilder builder = new StringBuilder("Komunikace{");
        //    builder.Append("idKomunikace:" + idKomunikace)
        //        .Append(",obsahKomunikace:" + obsahKomunikace)
        //        .Append(",datumVlozeni:" + datumVlozeni)
        //        .Append(",idHlaseni:" + idHlaseni);
        //    return builder.ToString();
        //}
    }


}
