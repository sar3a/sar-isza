﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISZA.Models.Rest
{
    public class UkolDto
    {
        public int idUkolu { get; set; }
        public string popis { get; set; }
        //public string resitel { get; set; }
        public DateTime terminSplneni { get; set; }
        //public DateTime datumSplneni { get; set; }
        public bool jePrijat { get; set; }
        public int idDohlizejici { get; set; }
        public int idResitel { get; set; }
    }
}
