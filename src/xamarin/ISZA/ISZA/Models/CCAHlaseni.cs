﻿namespace ISZA.Models
{
    public class CCAHlaseni
    {
        //Skutecne id v databazi
        public string IdHlaseni { get; set; }
        //N ty zaznam v roce
        public string Cislo { get; set; }
        // Danny rok
        public string Rok { get; set; }
        public string Druh { get; set; }
        public string Nazev { get; set; }
        public string System { get; set; }
        public string Obsah { get; set; }

        public string IdStr
        {
            get => Cislo + "/" + Rok;
        }
    }
}