﻿using System;
using System.Text;
using System.Reflection;
using Xamarin.Forms.Internals;

namespace ISZA.Models
{
    public class CCAKomunikace
    {
        public int Id { get; set; }
        //public int PoradoveCislo { get; set; }
        public string Obsah { get; set; }
        public int IdPuvodce { get; set; }
        public DateTime DatumVlozeni { get; set; }
        public string CCAHlaseniId { get; set; }
        public string CCAHlaseniCislo { get; set; }
        public string CCAHlaseniRok { get; set; }

        public string IdStr
        {
            get => "" + Id;
            set => Id = Int32.Parse(value);
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder("CCAKomunikace{");
            foreach (PropertyInfo propertyInfo in GetType().GetProperties())
            {
                string name = propertyInfo.Name;
                object value = propertyInfo.GetValue(this);
                builder.AppendFormat("{0}={1} ", name, value);
            }
            builder.Append("}");
            return builder.ToString();
        }
    }
}
