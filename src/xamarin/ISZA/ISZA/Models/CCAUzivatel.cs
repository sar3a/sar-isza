﻿namespace ISZA.Models
{
    public class CCAUzivatel
    {
        public int Id { get; set; }
        public string Jmeno { get; set; }
        public string Prijmeni { get; set; }
        public string KodOsoby { get; set; }
        public bool JeZamestnanec { get; set; }
        public string FirebaseToken { get; set; }

        public string Label => Jmeno + " " + Prijmeni;
    }
}