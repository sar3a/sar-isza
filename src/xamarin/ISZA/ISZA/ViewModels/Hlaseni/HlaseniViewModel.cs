﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using ISZA.Models;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using ISZA.Services.DataWorker.Hlaseni;

namespace ISZA.ViewModels.Hlaseni
{
    public class HlaseniViewModel: BaseViewModel
    {
        public ObservableCollection<CCAHlaseni> Hlaseni { get; set; }
        public Command NactiHlaseniCommand { get; set; }

        private IHlaseniDataWorker _idw => DependencyService.Get<IHlaseniDataWorker>() ?? new HlaseniDataWorker();

        private List<CCAHlaseni> _filtrovanaHlaseni;

        /// <inheritdoc />
        public HlaseniViewModel()
        {
            Title = "Hlášení";
            Hlaseni = new ObservableCollection<CCAHlaseni>();
            NactiHlaseniCommand = new Command(async () => await ExecuteNactiHlaseniCommand());
        }

        async Task ExecuteNactiHlaseniCommand()
        {
            if (IsBusy)
                return;
            IsBusy = true;

            try
            {
                Hlaseni.Clear();
                var items = await _idw.getOtevrenaHlaseni();
                items.ForEach(Hlaseni.Add);

                _filtrovanaHlaseni = Hlaseni.ToList();
                SeradHlaseni();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        public void FiltrujHlaseni(string text)
        {
            Hlaseni.Clear();
            _filtrovanaHlaseni
                .Where(r =>
                {
                    text = text.ToLower();
                    return r.Cislo.ToLower().Contains(text) || r.Nazev.ToLower().Contains(text) || r.Obsah.ToLower().Contains(text);
                })
                .ForEach(Hlaseni.Add);
            SeradHlaseni();
        }

        private void SeradHlaseni()
        {
            var hlaseni = new List<CCAHlaseni>(Hlaseni);
            Hlaseni.Clear();
            hlaseni.OrderByDescending(h=>h.IdStr).ForEach(Hlaseni.Add);
        }
    }
}
