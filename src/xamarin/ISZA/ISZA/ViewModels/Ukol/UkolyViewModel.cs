﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using ISZA.Models;
using ISZA.Services.DataWorker.Ukol;
using ISZA.Views.Ukol;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace ISZA.ViewModels.Ukol
{
    public class UkolyViewModel : BaseViewModel
    {
        public ObservableCollection<CCAUkol> Ukoly { get; set; }
        public Command NactiUkolyCommand { get; set; }
        public ETypUkolu AktualniTypUkolu { get; set; }

        private IUkolDataWorker _idw => DependencyService.Get<IUkolDataWorker>() ?? new UkolDataWorker();

        private List<CCAUkol> _dohlizeneUkoly, _zadaneUkoly;


        private readonly int _idUzivatele = App.signer.GetUzivatel().Id;

        /// <inheritdoc />
        public UkolyViewModel()
        {
            Ukoly = new ObservableCollection<CCAUkol>();
            AktualniTypUkolu = ETypUkolu.Vsechny;
            NactiUkolyCommand = new Command(async () => await ExecuteNactiUkolyCommand());

            InitSubscribers();
        }

        private void InitSubscribers()
        {
            // Zmena stavu ukolu
            MessagingCenter.Subscribe<UkolCell, CCAUkol>(this, "StavUkolu", async (obj, ukol) =>
            {
                Debug.WriteLine("Change " + ukol);
                await _idw.NastavStavUkolu(ukol);
            });

            // Ulozeni noveho ukolu
            MessagingCenter.Subscribe<NovyUkolPage, CCAUkol>(this, "PridatUkol", async (obj, ukol) =>
            {
                ukol.IdZadavatele = App.signer.GetUzivatel().Id;
                _dohlizeneUkoly.Add(ukol);
                if (AktualniTypUkolu == ETypUkolu.Vsechny || AktualniTypUkolu == ETypUkolu.Vytvorene)
                {
                    Ukoly.Add(ukol);
                    SeradUkoly();
                }

                await _idw.PridejAsync(ukol);
                Debug.WriteLine("Add new " + ukol);
            });
        }

        async Task ExecuteNactiUkolyCommand()
        {
            if (IsBusy)
                return;
            IsBusy = true;

            try
            {
                Ukoly.Clear();

                var dohlizene = await _idw.GetUkolyDohlizeneUzivatelem(_idUzivatele);
                var zadane = await _idw.GetUkolyZadaneUzivateli(_idUzivatele);
                var dohlizeneList = dohlizene as IList<CCAUkol> ?? dohlizene.ToList();
                var zadaneList = zadane as IList<CCAUkol> ?? zadane.ToList();

                _dohlizeneUkoly = dohlizeneList.ToList();
                _zadaneUkoly = zadaneList.ToList();

                ZmenUkoly(AktualniTypUkolu);

                //Debug.WriteLine(Ukoly[0]);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        public void FiltrujUkoly(ETypUkolu typ)
        {
            if (typ != AktualniTypUkolu)
            {
                AktualniTypUkolu = typ;
                ZmenUkoly(typ);
            }
        }

        private void ZmenUkoly(ETypUkolu typ)
        {
            Ukoly.Clear();
            switch (typ)
            {
                case ETypUkolu.Vytvorene:
                    _dohlizeneUkoly.ForEach(Ukoly.Add);
                    break;
                case ETypUkolu.Prirazene:
                    _zadaneUkoly.ForEach(Ukoly.Add);
                    break;
                case ETypUkolu.Vsechny:
                    _dohlizeneUkoly.ForEach(Ukoly.Add);
                    _zadaneUkoly.ForEach(Ukoly.Add);
                    break;
            }

            SeradUkoly();
        }

        private void SeradUkoly()
        {
            var ukoly = new List<CCAUkol>(Ukoly);
            Ukoly.Clear();
            ukoly.OrderBy(u => u.TerminSplneni).ForEach(Ukoly.Add);
        }
    }
}