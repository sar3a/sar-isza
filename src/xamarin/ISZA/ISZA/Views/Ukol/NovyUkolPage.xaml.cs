﻿using System;
using System.Collections;
using ISZA.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ISZA.Views.Ukol
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NovyUkolPage : ContentPage
    {
        public DateTime AktualniDatum => DateTime.Now;

        public CCAUkol Ukol { get; set; }

        public NovyUkolPage()
        {
            InitializeComponent();
            UzivatelPicker.ItemsList = (IList) App.uzivatelCache.GetCCAZamestnanecList();

            Ukol = new CCAUkol() {TerminSplneni = AktualniDatum};

            BindingContext = this;
        }

        private async void OnSaveButtonClicked(object sender, EventArgs e)
        {
            if (!(UzivatelPicker.SelectedItem is CCAUzivatel uzivatel))
            {
                await DisplayAlert(Title, "Vyplň řešitele", "OK");
            }
            else if (String.IsNullOrWhiteSpace(Ukol.Popis))
            {
                await DisplayAlert(Title, "Vyplň popis", "OK");
            }
            else
            {
                Ukol.IdResitele = uzivatel.Id;
                MessagingCenter.Send(this, "PridatUkol", Ukol);
                await Navigation.PopAsync(true);
            }
        }
    }
}