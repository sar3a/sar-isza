﻿using System;
using ISZA.Models;
using ISZA.ViewModels.Ukol;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ISZA.Views.Ukol
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UkolCell : ViewCell
    {
        private CCAUkol _ukol;
        private EStavUkolu _stavUkolu;

        public UkolCell()
        {
            InitializeComponent();
        }

        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();
            _ukol = BindingContext as CCAUkol;
            if (_ukol != null)
            {
                _stavUkolu = ToEStavUkolu(_ukol.JePrijat);

                StavUkoluPicker.IsEnabled = _ukol.JeResitel(App.signer.GetUzivatel().Id);
                StavUkoluPicker.SelectedItem = StavUkoluPicker.GetStav(_stavUkolu);

                StavUkoluPickerLabel.IsEnabled = StavUkoluPicker.IsEnabled;
            }
        }

        private void StavUkoluPicker_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            var stav = StavUkoluPicker.GetSelected();
            if (stav != null)
            {
                StavUkoluPickerLabel.Text = stav.Label;

                if (stav.Value != _stavUkolu)
                {
                    _stavUkolu = stav.Value;
                    _ukol.JePrijat = ToBool(_stavUkolu);
                    MessagingCenter.Send(this, "StavUkolu", _ukol);
                }
            }
        }
        
        private void StavUkoluPickerLabel_OnTapped(object sender, EventArgs e)
        {
            StavUkoluPicker.Focus();
        }

        private static EStavUkolu ToEStavUkolu(bool b)
        {
            return b ? EStavUkolu.Prijat : EStavUkolu.Neprijat;
        }

        private static bool ToBool(EStavUkolu eStavUkolu)
        {
            return eStavUkolu == EStavUkolu.Prijat;
        }
    }
}