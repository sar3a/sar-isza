﻿using System;
using ISZA.ViewModels.Ukol;
using ISZA.Views.Helpers;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ISZA.Views.Ukol
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UkolyPage : ContentPage
    {
        private UkolyViewModel viewModel;

        public UkolyPage()
        {
            InitializeComponent();
            BindingContext = viewModel = new UkolyViewModel();
            TypUkoluPicker.SelectedItem = TypUkoluPicker.GetTyp(viewModel.AktualniTypUkolu);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            // Loads reports if list is empty
            if (viewModel.Ukoly.Count == 0)
                viewModel.NactiUkolyCommand.Execute(null);
        }

        private async void NovyUkol_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new NovyUkolPage(), true);
        }

        private void SearchBar_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            //viewModel.FiltrujUkoly(UkolySearchBar.Text);
        }

        private void TypUkoluPicker_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (TypUkoluPicker.SelectedIndex > -1)
            {
                var typ = TypUkoluPicker.SelectedItem as Labeled<ETypUkolu>;
                TypUkoluPickerLabel.Text = typ.Label;
                viewModel.FiltrujUkoly(typ.Value);
            }
        }

        private void UkolyListView_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (e == null) return;
            UkolyListView.SelectedItem = null;
        }

        private void TypUkoluPickerLabel_OnTapped(object sender, EventArgs e)
        {
            TypUkoluPicker.Focus();
        }
    }
}