﻿using System;
using ISZA.Models;
using ISZA.Services.Authentication;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ISZA.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        // dependency injection vytvori odpovidajici servicu (z registrovanch) ( ?? defaultne LoginService())
        private IAuthenticationService authenticationService =>
            DependencyService.Get<IAuthenticationService>() ?? new LoginService();

        public LoginPage()
        {
            InitializeComponent();
        }

        private async void OnLoginButtonClicked(object sender, EventArgs e)
        {
            var user = new CCAUzivatel
            {
                KodOsoby = UsernameEntry.Text,
            };

            bool isLogged = await App.signer.PrihlasMe(user.KodOsoby);

            if (isLogged)
            {
                Navigation.InsertPageBefore(new MainPage(), this);
                await Navigation.PopAsync();
            }
            else
            {
                MessageLabel.Text = "Přihlášení se nezdařilo";
                PasswordEntry.Text = string.Empty;
            }
        }

    }
}