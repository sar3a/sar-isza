﻿using System.Collections;
using ISZA.Models;
using Xamarin.Forms;

namespace ISZA.Views.Helpers.CustomPicker
{
    public abstract class ACustomPicker<T> : Picker
    {
        protected ACustomPicker() : base()
        {
        }

        /// <summary>
        /// Pouzit pro nastaveni zdroje namisto ItemsSource
        /// </summary>
        public IList ItemsList
        {
            get => ItemsSource;
            set
            {
                var oldSource = ItemsSource;
                ItemsSource = value;
                OnItemsListChanged(oldSource, ItemsSource);
            }
        }

        protected virtual void OnItemsListChanged(IList oldSource, IList newSource)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>zvolenehou polozku nebo null, pokud nebyla vybrana</returns>
        public T GetSelected()
        {
            return (T) SelectedItem;
        }
    }
}