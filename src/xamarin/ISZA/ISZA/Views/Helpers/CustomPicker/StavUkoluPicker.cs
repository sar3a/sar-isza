﻿using System.Collections.Generic;
using ISZA.ViewModels.Ukol;
using ISZA.Views.Helpers.CustomPicker;
using Xamarin.Forms;

namespace ISZA.Views.Helpers
{
    public class StavUkoluPicker : ACustomPicker<Labeled<EStavUkolu>>
    {
        private static List<Labeled<EStavUkolu>> _stavy = new List<Labeled<EStavUkolu>>(new[]
        {
            new Labeled<EStavUkolu>()
            {
                Label = "Přijat",
                Value = EStavUkolu.Prijat
            },
            new Labeled<EStavUkolu>()
            {
                Label = "Nepřijat",
                Value = EStavUkolu.Neprijat
            }
        });

        public StavUkoluPicker() : base()
        {
            ItemsList = _stavy;
        }

        public Labeled<EStavUkolu> GetStav(EStavUkolu eStav)
        {
            foreach (Labeled<EStavUkolu> stav in ItemsSource)
            {
                if (stav.Value == eStav) return stav;
            }

            return new Labeled<EStavUkolu>() {Value = eStav};
        }
    }
}