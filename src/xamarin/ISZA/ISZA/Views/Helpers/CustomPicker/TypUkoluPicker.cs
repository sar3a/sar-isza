﻿using System.Collections.Generic;
using ISZA.ViewModels.Ukol;

namespace ISZA.Views.Helpers.CustomPicker
{
    public class TypUkoluPicker : ACustomPicker<Labeled<ETypUkolu>>
    {
        private static List<Labeled<ETypUkolu>> _typy = new List<Labeled<ETypUkolu>>(new[]
        {
            new Labeled<ETypUkolu>() {Label = "Všechny", Value = ETypUkolu.Vsechny},
            new Labeled<ETypUkolu>() {Label = "Moje úkoly", Value = ETypUkolu.Prirazene},
            new Labeled<ETypUkolu>() {Label = "Mnou zadané", Value = ETypUkolu.Vytvorene}
        });

        public TypUkoluPicker() : base()
        {
            ItemsList = _typy;
        }

        public Labeled<ETypUkolu> GetTyp(ETypUkolu eTyp)
        {
            foreach (Labeled<ETypUkolu> typ in ItemsSource)
            {
                if (typ.Value == eTyp) return typ;
            }

            return new Labeled<ETypUkolu>() {Value = eTyp};
        }
    }
}