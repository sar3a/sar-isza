﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ISZA.Models;

namespace ISZA.Views.Helpers.CustomPicker
{
    public class UzivatelPicker : ACustomPicker<CCAUzivatel>
    {
        public UzivatelPicker() : base()
        {
            ItemsList = App.uzivatelCache.GetCCAUzivatelList().ToList();
        }

        protected override void OnItemsListChanged(IList oldSource, IList newSource)
        {
            base.OnItemsListChanged(oldSource, newSource);
            ItemsSource = ((List<CCAUzivatel>) newSource).OrderBy(u => u.Label).ToList();
        }
    }
}