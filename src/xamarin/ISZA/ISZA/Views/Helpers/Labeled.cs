﻿namespace ISZA.Views.Helpers
{
    public class Labeled<T>
    {
        public string Label { get; set; }
        public T Value { get; set; }
        
    }
}
