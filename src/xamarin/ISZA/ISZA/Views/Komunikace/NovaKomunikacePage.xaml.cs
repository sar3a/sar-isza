﻿using System;
using System.Diagnostics;
using ISZA.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ISZA.Services.DataWorker.Komunikace;

namespace ISZA.Views.Komunikace
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NovaKomunikacePage : ContentPage
    {
        private IKomunikaceDataWorker _kdw => DependencyService.Get<IKomunikaceDataWorker>() ?? new KomunikaceDataWorker();

        public CCAKomunikace NovaCCAKomunikace { get; }
        public string ViditelneCCAHlaseniCislo { get; set; }
        public DateTime AktualniDatum => DateTime.Now;
        public TimeSpan AktualniCas => new TimeSpan(AktualniDatum.Hour, AktualniDatum.Minute, AktualniDatum.Second);

        public NovaKomunikacePage() : this(new CCAKomunikace())
        {
        }

        public NovaKomunikacePage(CCAKomunikace novaKomunikace)
        {
            NovaCCAKomunikace = novaKomunikace;
            //Debug.WriteLine(NovaCCAKomunikace);
            ViditelneCCAHlaseniCislo = NovaCCAKomunikace.CCAHlaseniCislo;

            InitializeComponent();

        }

        private async void OnSaveButtonClicked(object sender, EventArgs e)
        {
            CCAUzivatel puvodce = UzivatelPicker.GetSelected();
            if (puvodce is null)
            {
                await DisplayAlert(Title, "Vyplň kdo volal", "OK");
                return;
            }
            NovaCCAKomunikace.IdPuvodce = puvodce.Id;
            NovaCCAKomunikace.DatumVlozeni = new DateTime(
                KomunikaceDatePicker.Date.Year,
                KomunikaceDatePicker.Date.Month,
                KomunikaceDatePicker.Date.Day,
                KomunikaceTimePicker.Time.Hours,
                KomunikaceTimePicker.Time.Minutes,
                KomunikaceTimePicker.Time.Seconds);
            NovaCCAKomunikace.Obsah = KomunikaceObsahEditor.Text;
            //Debug.WriteLine(NovaCCAKomunikace);
            var result = await _kdw.PridejAsync(NovaCCAKomunikace);
            if (result)
            {
                await Navigation.PopAsync();
            }
            else
            {
                // SELHANI
                await DisplayAlert(Title, "Chyba při ukldání", "Zavřít");
            }
        }
    }
  
}