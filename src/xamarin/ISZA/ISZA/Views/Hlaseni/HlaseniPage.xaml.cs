﻿using ISZA.Models;
using ISZA.ViewModels.Hlaseni;
using ISZA.Views.Komunikace;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ISZA.Views.Hlaseni
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HlaseniPage : ContentPage
    {
        private HlaseniViewModel viewModel;

        public HlaseniPage()
        {
            InitializeComponent();

            BindingContext = viewModel = new HlaseniViewModel();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            // Loads reports if list is empty
            if (viewModel.Hlaseni.Count == 0)
                viewModel.NactiHlaseniCommand.Execute(null);
        }

        private async void HlaseniCell_OnAddComunicationClicked(object sender, HlaseniCell.HlaseniEventArgs eventArgs)
        {
            CCAKomunikace komunikace = new CCAKomunikace();
            
            //int idHlaseni = 1;
            //if (!string.IsNullOrEmpty(eventArgs.Report.IdHlaseni))
            //{
            //    idHlaseni = Int32.Parse(eventArgs.Report.IdHlaseni);
            //}
            komunikace.CCAHlaseniId = eventArgs.Hlaseni.IdHlaseni;
            komunikace.CCAHlaseniCislo = eventArgs.Hlaseni.Cislo;
            komunikace.CCAHlaseniRok = eventArgs.Hlaseni.Rok;
            await Navigation.PushAsync(new NovaKomunikacePage(komunikace));
        }

        private void SearchBar_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            viewModel.FiltrujHlaseni(HlaseniSearchBar.Text);
        }
    }
}
