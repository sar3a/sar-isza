﻿using System;
using ISZA.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ISZA.Views.Hlaseni
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HlaseniCell : ViewCell
    {
        public event EventHandler<HlaseniEventArgs> PridatKomunikaciClicked;

        public HlaseniCell()
        {
            InitializeComponent();
        }

        private void PridatKomunikaciButtonClicked(object sender, EventArgs e)
        {
            CCAHlaseni hlaseni = new CCAHlaseni()
            {
                IdHlaseni = HlaseniHiddenIdLabel.Text,
                Cislo = HlaseniIdLabel.Text.Split('/')[0],
                Rok = HlaseniIdLabel.Text.Split('/')[1],
            }; 
            this.PridatKomunikaciClicked?.Invoke(sender, new HlaseniEventArgs() { Hlaseni = hlaseni });
        }

        public class HlaseniEventArgs : EventArgs
        {
            public CCAHlaseni Hlaseni;
        }
    }
}