﻿using ISZA.Views.Hlaseni;
using ISZA.Views.Ukol;
using Xamarin.Forms;

namespace ISZA.Views
{
    public class MainPage : TabbedPage
    {
        public MainPage()
        {
            Page hlaseniPage, ukolyPage;

            switch (Device.RuntimePlatform)
            {
                case Device.iOS:
                    hlaseniPage = new NavigationPage(new HlaseniPage());
                    ukolyPage = new NavigationPage(new UkolyPage());
                    break;
                default:
                    hlaseniPage = new HlaseniPage();
                    ukolyPage = new UkolyPage() ;
                    break;
            }

            
            Children.Add(hlaseniPage);
            Children.Add(ukolyPage);

            Title = Children[0].Title;
        }

        protected override void OnCurrentPageChanged()
        {
            base.OnCurrentPageChanged();
            Title = CurrentPage?.Title ?? string.Empty;
        }
    }
}